from django.urls import path

from . import views

# PLEASE don't use re_path unless you have a valid reason to do so!
urlpatterns = [
    path('teacher/dashboard/', views.teacher_dashboard_view, name='teacher_dashboard_view'),
    path('teacher/dashboard/manage_quizzes/', views.teacher_manage_quizzes_view, name='teacher_manage_quizzes_view'),
    path('teacher/dashboard/manage_quizzes/modify/', views.teacher_modify_quiz_view, name='teacher_modify_quiz_view'),
    path('student/homepage/', views.student_dashboard_view, name='student_dashboard_view'),
    path('student/homepage/quizzes/', views.student_quizzes_view, name='student_quizzes_view'),
    path('student/homepage/quizzes/confirm/', views.student_confirm_quiz_view, name='student_confirm_quiz_view'),
    path('student/homepage/results/', views.student_results_view, name='student_results_view'),
    path('student/homepage/results/attempts/', views.students_attempts_view, name='students_attempts_view'),
    path('quiz-page/normal/<int:quiz_id>/', views.student_normal_quiz_view, name='student_normal_quiz_view'),
    path('quiz-page/adaptive/<int:quiz_id>/', views.student_adaptive_quiz_view, name='student_adaptive_quiz_view'),
    path('quiz-page/unseen/<int:quiz_id>/', views.student_unseen_quiz_view, name='student_unseen_quiz_view'),
    path('quiz-page/render_next_question', views.render_next_question, name='render_next_question'),
    path('quiz-page/previously_wrong/<int:quiz_id>/', views.student_previously_wrong_quiz_view, name='student_previously_wrong_quiz_view'),
    path('quiz-page/get_remaining_questions', views.get_remaining_questions, name='get_remaining_questions'),

    path('quiz-page/get_categories', views.category_result, name='get_categories_results'),

    path('quiz-page/get_stimulus_data', views.get_stimulus_data, name='adaptive_get_stimulus_data'),
    path('quiz-page/render_next_stimulus', views.render_next_stimulus, name='render_next_stimulus'),

    path('teacher/404', views.error404_test_teacher, name='error404_test_teacher'),
    path('student/404', views.error404_test_student, name='error404_test_student'),
]
