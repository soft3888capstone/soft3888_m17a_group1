# VIDEO/CODE USED/COPIED FROM
# https://www.youtube.com/watch?v=0MrgsYswT1c


from django.test import TestCase
from django.urls import reverse, resolve

from quizengine.views import (teacher_dashboard_view,
                              teacher_manage_quizzes_view,
                              student_dashboard_view,
                              student_quizzes_view,
                              student_confirm_quiz_view,
                              student_results_view,
                              students_attempts_view,
                              render_next_question,
                              get_remaining_questions,
                              get_stimulus_data)


class TestUrls(TestCase):

    def test_teacher_dashboard_view_resolves(self):
        url = reverse('teacher_dashboard_view')
        self.assertEquals(resolve(url).func, teacher_dashboard_view)

    def test_teacher_manage_quizzes_view_resolves(self):
        url = reverse('teacher_manage_quizzes_view')
        self.assertEquals(resolve(url).func, teacher_manage_quizzes_view)

    def test_student_dashboard_view_resolves(self):
        url = reverse('student_dashboard_view')
        self.assertEquals(resolve(url).func, student_dashboard_view)

    def test_student_quizzes_view_resolves(self):
        url = reverse('student_quizzes_view')
        self.assertEquals(resolve(url).func, student_quizzes_view)

    def test_student_confirm_quiz_view(self):
        url = reverse('student_confirm_quiz_view')
        self.assertEquals(resolve(url).func, student_confirm_quiz_view)

    def test_student_results_view(self):
        url = reverse('student_results_view')
        self.assertEquals(resolve(url).func, student_results_view)

    def test_student_attempts_view(self):
        url = reverse('students_attempts_view')
        self.assertEquals(resolve(url).func, students_attempts_view)

    # TODO left this out first, since it fails due to regex in url

    # def test_student_normal_quiz_view(self):
    #     url = reverse('student_normal_quiz_view')
    #     self.assertEquals(resolve(url).func, student_normal_quiz_view)
    #
    # def test_student_adaptive_quiz_view(self):
    #     url = reverse('student_adaptive_quiz_view')
    #     self.assertEquals(resolve(url).func, student_adaptive_quiz_view)
    #
    # def test_student_unseen_quiz_view(self):
    #     url = reverse('student_unseen_quiz_view')
    #     self.assertEquals(resolve(url).func, student_unseen_quiz_view)

    def test_render_next_question(self):
        url = reverse('render_next_question')
        self.assertEquals(resolve(url).func, render_next_question)

    def test_get_remaining_questions(self):
        url = reverse('get_remaining_questions')
        self.assertEquals(resolve(url).func, get_remaining_questions)

    def test_adaptive_get_stimulus_data(self):
        url = reverse('adaptive_get_stimulus_data')
        self.assertEquals(resolve(url).func, get_stimulus_data)
