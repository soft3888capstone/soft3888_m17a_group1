# CODE USED/COPIED
# https://stackoverflow.com/questions/7669931/django-break-up-a-request-path-into-each-word

import json
import math
import random
from random import randint

from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect

from quizconfig.models import AdaptiveQuiz
from quizconfig.models import AdaptiveQuizResult
from quizconfig.models import CanvasQuiz
from quizconfig.models import CanvasQuizResult
from quizconfig.models import NormalQuiz
from quizconfig.models import NormalQuizResult
from quizconfig.models import PreviouslyWrongQuiz
from quizconfig.models import PreviouslyWrongQuizResult
from quizconfig.models import QuizAnswers
from quizconfig.models import UnseenQuestionsQuiz
from quizconfig.models import UnseenQuestionsQuizResult
from quizengine.models import Answer
from quizengine.models import Difficulty
from quizengine.models import Question
from quizengine.models import QuizDistribution
from quizengine.models import Stimulus
from lti.models import UserType

difficulty_list = ["basic", "easier", "easy", "hard" "harder", "challenge"]
score = 250


@permission_required('lti.is_student', raise_exception=True)
def render_next_question(request):
    quiz_type = request.POST.get("quiz_type")
    if quiz_type == "normal_quiz":
        return render_normal_question(request)
    elif quiz_type == "adaptive_quiz":
        return render_adaptive_question(request)
    elif quiz_type == "unseen_questions_quiz":
        return render_unseen_question(request)
    elif quiz_type == "prev_wrong_quiz":
        return render_prev_wrong_question(request)
    return None

    # TODO Old stuff, delete later
    # idx = int(request.POST.get('idx'))
    # print(idx)
    # quiz_id = int(request.POST.get('quiz_id'))
    # question_only = list(NormalQuiz.objects.get(pk=quiz_id).questions.all())[idx]
    # no_of_question = NormalQuiz.objects.get(pk=quiz_id).no_of_questions
    # question = [question_only, list(Answer.objects.filter(question=question_only))]
    # answers = request.POST.get('answers')
    # return render(request, 'student/question.html', {"question": question, "no_of_question": no_of_question})


@permission_required('lti.is_student', raise_exception=True)
def render_next_stimulus(request):
    idx = int(request.POST.get('idx'))
    print(idx)
    quiz_id = int(request.POST.get('quiz_id'))
    question_only = list(NormalQuiz.objects.get(pk=quiz_id).questions.all())[idx]
    no_of_question = int(NormalQuiz.objects.get(pk=quiz_id).no_of_questions)
    question = [question_only, list(Answer.objects.filter(question=question_only))]
    answers = request.POST.get('answers')
    stimulus = Stimulus.objects.filter(questions=question_only)
    if stimulus is None:
        stimulus = question

    return render(request, 'student/stimulus.html', {"stimulus": stimulus, })


@permission_required('lti.is_student', raise_exception=True)
def get_remaining_questions(request):
    count = 0

    if request.GET.get("quiz_type") == "adaptive_quiz":
        count = int(AdaptiveQuiz.objects.get(pk=int(request.GET.get("quiz_id"))).no_of_questions)
    elif request.GET.get("quiz_type") == "normal_quiz":
        count = int(NormalQuiz.objects.get(pk=int(request.GET.get("quiz_id"))).no_of_questions)
    elif request.GET.get("quiz_type") == "unseen_questions_quiz":
        count = int(UnseenQuestionsQuiz.objects.get(pk=int(request.GET.get("quiz_id"))).no_of_questions)
    elif request.GET.get("quiz_type") == "prev_wrong_quiz":
        count = int(PreviouslyWrongQuiz.objects.get(pk=int(request.GET.get("quiz_id"))).no_of_questions)

    print(request.GET.get("quiz_type"))
    print(count - int(request.GET.get("idx")))
    ret = {"ret": count - int(request.GET.get("idx"))}
    print(ret)

    return HttpResponse(json.dumps(ret))


def get_answers(answers):
    new_answers = answers.replace("\"", "")
    new_answers = new_answers[1:-1]
    my_list = new_answers.split(",")
    return my_list


# source: https://stackoverflow.com/questions/8595973/truncate-to-three-decimals-in-python
def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


def get_final_score(answers, idx, quiz_object, user_id):
    return_list = []

    quiz_result_table = None

    # TODO: ADD OTHER QUIZ_TYPE
    if type(quiz_object) == NormalQuiz:
        quiz_result_table = NormalQuizResult.objects.create(quiz=quiz_object, student_id=user_id, score=0)
    elif type(quiz_object) == AdaptiveQuiz:
        quiz_result_table = AdaptiveQuizResult.objects.create(quiz=quiz_object, student_id=user_id, score=0)
    elif type(quiz_object) == UnseenQuestionsQuiz:
        quiz_result_table = UnseenQuestionsQuizResult.objects.create(quiz=quiz_object, student_id=user_id, score=0)
    elif type(quiz_object) == PreviouslyWrongQuiz:
        quiz_result_table = PreviouslyWrongQuizResult.objects.create(quiz=quiz_object, student_id=user_id, score=0)

    final_score = 0;

    for i in range(0, idx):
        answer = Answer.objects.get(id=answers[i])
        question = Question.objects.get(id=answer.question_id)

        quiz_answer_object = QuizAnswers.objects.create(question=question, selected_answer=answer)
        quiz_result_table.answers.add(quiz_answer_object)

        correct_answer = Answer.objects.filter(question_id=question.id).filter(weighting=100.0)[0]

        if (answer == correct_answer):
            final_score += 1

        new_list = [question, answer, correct_answer]
        return_list.append(new_list)

    quiz_result_table.score = final_score
    quiz_result_table.save()

    final_score = int(final_score * 100 / quiz_object.no_of_questions)

    score_and_questions = (final_score, return_list)

    return score_and_questions


@permission_required('lti.is_student', raise_exception=True)
def category_result(request):
    selected_answer = request.POST.get('selected_answer')
    correct_answer = request.POST.get('correct_answer')

    selected_answer_list = get_answers(selected_answer)
    correct_answer_list = get_answers(correct_answer)

    category_name_list = []

    for key, value in enumerate(selected_answer_list):
        category_obj_name = Answer.objects.get(pk=int(value)).question.category.name
        is_correct = False

        if value == correct_answer_list[key]:
            is_correct = True

        found = False

        for j in category_name_list:
            if j["categ_name"] == category_obj_name:
                found = True
                if is_correct:
                    j["correct"] += 1
                else:
                    j["incorrect"] += 1

        if not found:
            categ_dict = {
                "categ_name": category_obj_name,
                "correct": 1 if is_correct else 0,
                "incorrect": 0 if is_correct else 1,
            }
            category_name_list.append(categ_dict)

    return HttpResponse(json.dumps(category_name_list))


# used for getting data from adaptive_question, this global is used in get_stimulus_data
# TODO need to find a cleaner way of not using globals
global_question_only = None


# TODO: force login!!
@permission_required('lti.is_student', raise_exception=True)
def render_unseen_question(request):
    idx = int(request.POST.get('idx'))
    quiz_id = int(request.POST.get('quiz_id'))
    quiz_object = UnseenQuestionsQuiz.objects.get(pk=quiz_id)
    count = int(quiz_object.no_of_questions)
    answers = request.POST.get('answers')
    end_early = int(request.POST.get('end_early'))
    answer_list = get_answers(answers)
    user_id = None

    if answers == "[]":
        return render(request, 'student/end_of_quiz.html', {"final_score": 0, "answers": []})

    if request.user.is_authenticated:
        user_id = request.user.pk

    if idx >= count or end_early:
        answers = get_answers(answers)
        score_tuple = get_final_score(answers, idx, quiz_object, user_id)
        return_list = score_tuple[1]
        final_score = score_tuple[0]

        for answer in answers:
            ans = Answer.objects.get(pk=answer)
            question = ans.question
            question.seen_users.add(User.objects.get(pk=user_id))

        return render(request, 'student/end_of_quiz.html', {"final_score": final_score, "answers": return_list})

    subject = quiz_object.subject

    # user = User.objects.get(pk = user_id)
    all_questions = Question.objects.all()
    user_total_seen_questions = Question.objects.filter(seen_users=user_id).filter(
        subject=UnseenQuestionsQuiz.objects.get(pk=quiz_id).subject)

    user_total_unseen_questions = [x for x in all_questions if x not in user_total_seen_questions]

    all_questions = Question.objects.filter(subject=subject)
    seen_questions = []

    for answer in answer_list:
        seen_questions.append(Answer.objects.get(pk=int(answer)).question)

    unseen_questions = [x for x in all_questions if x not in seen_questions]

    # extra_unseen_questions = [x for x in user_total_seen_questions if x in unseen_questions]
    # extra_unseen_questions = [x for x in user_total_unseen_questions if x in unseen_questions]
    extra_unseen_questions = [x for x in user_total_unseen_questions if x not in seen_questions]

    next_question_index = randint(0, len(extra_unseen_questions) - 1)

    question_only = extra_unseen_questions[next_question_index]
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))

    global global_question_only
    global_question_only = question_only
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]

    return render(request, 'student/question.html', {"question": question})


@permission_required('lti.is_student', raise_exception=True)
def render_prev_wrong_question(request):
    quiz_id = int(request.POST.get('quiz_id'))
    answers = request.POST.get('answers')
    end_early = int(request.POST.get('end_early'))

    if (answers == "[]"):
        return render(request, 'student/end_of_quiz.html', {"final_score": 0, "answers": []})

    answer_list = get_answers(answers)
    answer_id = answer_list[-1]
    answer_obj = Answer.objects.get(id=answer_id)
    count = PreviouslyWrongQuiz.objects.get(pk=quiz_id).no_of_questions
    quiz_object = PreviouslyWrongQuiz.objects.get(pk=quiz_id)
    user_id = 1

    if request.user.is_authenticated:
        user_id = request.user.pk

    ctr = int(request.POST.get('idx'))

    if ctr >= count or end_early:
        answers = get_answers(answers)
        score_tuple = get_final_score(answers, ctr, quiz_object, user_id)
        return_list = score_tuple[1]
        final_score = score_tuple[0]

        for answer in answers:
            ans = Answer.objects.get(pk=answer)
            question = ans.question
            question.seen_users.add(User.objects.get(pk=user_id))

        return render(request, 'student/end_of_quiz.html', {"final_score": final_score, "answers": return_list})

    idx = math.trunc((score / 100.0)) - 1
    if idx < 0:
        idx = 0

    normal_quiz_result_for_user = NormalQuizResult.objects.filter(student_id=user_id)
    adaptive_quiz_result_for_user = AdaptiveQuizResult.objects.filter(student_id=user_id)
    unseen_quiz_result_for_user = UnseenQuestionsQuizResult.objects.filter(student_id=user_id)
    prev_wrong_quiz_result_for_user = PreviouslyWrongQuizResult.objects.filter(student_id=user_id)

    subject_id = PreviouslyWrongQuiz.objects.get(pk=quiz_id).subject_id

    incorrect_question_list = []

    get_incorrect_quiz_question(normal_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(adaptive_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(unseen_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(prev_wrong_quiz_result_for_user, incorrect_question_list)

    seen_questions = []
    for answer in answer_list:
        seen_questions.append(Answer.objects.get(pk=int(answer)).question)

    unseen_incorrect_questions = [x for x in incorrect_question_list if x not in seen_questions]

    next_question_index = randint(0, len(unseen_incorrect_questions) - 1)

    question_only = unseen_incorrect_questions[next_question_index]
    global global_question_only
    global_question_only = question_only
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]
    return render(request, 'student/question.html', {"question": question})


@permission_required('lti.is_student', raise_exception=True)
def render_adaptive_question(request):
    quiz_id = int(request.POST.get('quiz_id'))
    score = int(request.POST.get('score'))
    points = math.trunc(float(request.POST.get('points')))
    answers = request.POST.get('answers')
    end_early = int(request.POST.get('end_early'))

    if (answers == "[]"):
        return render(request, 'student/end_of_quiz.html', {"final_score": 0, "answers": []})

    answer_list = get_answers(answers)
    answer_id = answer_list[-1]
    answer_obj = Answer.objects.get(id=answer_id)
    count = AdaptiveQuiz.objects.get(pk=quiz_id).no_of_questions
    quiz_object = AdaptiveQuiz.objects.get(pk=quiz_id)
    user_id = 1

    if request.user.is_authenticated:
        user_id = request.user.pk

    if answer_obj.weighting == 100.0:
        score += points
    else:
        score -= points

    ctr = int(request.POST.get('idx'))

    if ctr >= count or end_early:
        answers = get_answers(answers)
        score_tuple = get_final_score(answers, ctr, quiz_object, user_id)
        return_list = score_tuple[1]
        final_score = score_tuple[0]

        for answer in answers:
            ans = Answer.objects.get(pk=answer)
            question = ans.question
            question.seen_users.add(User.objects.get(pk=user_id))

        return render(request, 'student/end_of_quiz.html', {"final_score": final_score, "answers": return_list})

    idx = math.trunc((score / 100.0)) - 1
    if idx < 0:
        idx = 0

    next_difficulty = difficulty_list[idx]
    subject_id = AdaptiveQuiz.objects.get(pk=quiz_id).subject_id
    difficulty_obj = Difficulty.objects.filter(subject=subject_id).get(name=next_difficulty)

    all_questions = Question.objects.filter(difficulty=difficulty_obj)
    seen_questions = []

    for answer in answer_list:
        seen_questions.append(Answer.objects.get(pk=int(answer)).question)

    unseen_questions = [x for x in all_questions if x not in seen_questions]
    next_question_index = randint(0, len(unseen_questions) - 1)

    question_only = unseen_questions[next_question_index]

    global global_question_only
    global_question_only = question_only
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]
    return render(request, 'student/question.html', {"question": question})


# Added this function because there is no way to separate loading adaptive and normal rendering function
@permission_required('lti.is_student', raise_exception=True)
def get_stimulus_data(request):
    stimulus = Stimulus.objects.filter(questions=global_question_only)
    # if stimulus is None:
    #     stimulus = question

    return render(request, 'student/stimulus.html', {"stimulus": stimulus})


@permission_required('lti.is_student', raise_exception=True)
def render_normal_question(request):
    idx = int(request.POST.get('idx'))
    quiz_id = int(request.POST.get('quiz_id'))
    count = NormalQuiz.objects.get(pk=quiz_id).no_of_questions
    answers = request.POST.get('answers')
    end_early = int(request.POST.get('end_early'))
    user_id = 1
    quiz_object = NormalQuiz.objects.get(pk=quiz_id)

    if (answers == "[]"):
        return render(request, 'student/end_of_quiz.html', {"final_score": 0, "answers": []})

    if request.user.is_authenticated:
        user_id = request.user.pk

    if idx >= count or end_early:
        answers = get_answers(answers)
        score_tuple = get_final_score(answers, idx, quiz_object, user_id)
        return_list = score_tuple[1]
        final_score = score_tuple[0]

        for answer in answers:
            ans = Answer.objects.get(pk=answer)
            question = ans.question
            question.seen_users.add(User.objects.get(pk=user_id))

        return render(request, 'student/end_of_quiz.html', {"final_score": final_score, "answers": return_list})

    question_only = list(NormalQuiz.objects.get(pk=quiz_id).questions.all())[idx]

    print(answers)
    answers = get_answers(answers)
    prev_selected = 0
    if(idx < len(answers)):
        prev_selected = answers[idx]
    else:
        prev_selected = -1

    global global_question_only
    global_question_only = question_only
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)

    question = [question_only, shuffled_answers, prev_selected]

    return render(request, 'student/question.html', {"question": question}, )


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_dashboard_view(request):
    template_data = {
        'title': 'Dashboard',
        'path': request.path.split('/')[:-1]
    }
    # see code used break up a request.path into each word
    return render(request, 'teacher/dashboard.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_manage_quizzes_view(request):
    try:
        del request.session['distribution_data']
        del request.session['quiz_data']
        del request.session['quiz_id']
    except KeyError:
        True

    normal_quiz_data = []
    canvas_quiz_data = []
    adaptive_quiz_data = []
    previously_wrong_quiz_data = []
    unseen_questions_quiz_data = []

    if request.method == 'POST' and 'quiz_type' in request.POST and 'quiz_key' in request.POST:
        quiz_data = {}
        distribution_data = {}
        quiz_id = {}
        if request.POST['quiz_type'] == 'normal':
            try:
                quiz = NormalQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_id = {'pk': quiz.pk,
                           'type': request.POST['quiz_type']
                           }
                quiz_data = {'Quiz Type': 'Normal Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Difficulty': quiz.difficulty.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
                distribution_pks = quiz.distributions.all().values_list('pk', flat=True)
                for pk in distribution_pks:
                    try:
                        distribution = QuizDistribution.objects.get(pk=pk)
                        distribution_data[distribution.category.name.title()] = distribution.percentage
                    except QuizDistribution.DoesNotExist:
                        return redirect('teacher_manage_quizzes_view')
            except NormalQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'canvas':
            try:
                quiz = CanvasQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_id = {'pk': quiz.pk,
                           'type': request.POST['quiz_type']
                           }
                quiz_length = quiz.questions.all().values_list('pk', flat=True)
                quiz_data = {'Quiz Type': 'Canvas Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': len(quiz_length)
                             }
            except CanvasQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'adaptive':
            try:
                quiz = AdaptiveQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_id = {'pk': quiz.pk,
                           'type': request.POST['quiz_type']
                           }
                quiz_data = {'Quiz Type': 'Adaptive Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
            except AdaptiveQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'previously_wrong':
            try:
                quiz = PreviouslyWrongQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_id = {'pk': quiz.pk,
                           'type': request.POST['quiz_type']
                           }
                quiz_data = {'Quiz Type': 'Previously Wrong Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
            except PreviouslyWrongQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'unseen':
            try:
                quiz = UnseenQuestionsQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_id = {'pk': quiz.pk,
                           'type': request.POST['quiz_type']
                           }
                quiz_data = {'Quiz Type': 'Unseen Questions Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
            except UnseenQuestionsQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')

        request.session['distribution_data'] = distribution_data
        request.session['quiz_data'] = quiz_data
        request.session['quiz_id'] = quiz_id
        return redirect("teacher_modify_quiz_view")

    for pk in NormalQuiz.objects.filter().values_list('id', flat=True):
        quiz = NormalQuiz.objects.get(pk=pk)
        normal_quiz_data.append({'pk': quiz.pk,
                                 'name': quiz.name,
                                 'subject': quiz.subject})

    for pk in CanvasQuiz.objects.filter().values_list('id', flat=True):
        quiz = CanvasQuiz.objects.get(pk=pk)
        canvas_quiz_data.append({'pk': quiz.pk,
                                 'name': quiz.name,
                                 'subject': quiz.subject})

    for pk in AdaptiveQuiz.objects.filter().values_list('id', flat=True):
        quiz = AdaptiveQuiz.objects.get(pk=pk)
        adaptive_quiz_data.append({'pk': quiz.pk,
                                   'name': quiz.name,
                                   'subject': quiz.subject})

    for pk in PreviouslyWrongQuiz.objects.filter().values_list('id', flat=True):
        quiz = PreviouslyWrongQuiz.objects.get(pk=pk)
        previously_wrong_quiz_data.append({'pk': quiz.pk,
                                           'name': quiz.name,
                                           'subject': quiz.subject})

    for pk in UnseenQuestionsQuiz.objects.filter().values_list('id', flat=True):
        quiz = UnseenQuestionsQuiz.objects.get(pk=pk)
        unseen_questions_quiz_data.append({'pk': quiz.pk,
                                           'name': quiz.name,
                                           'subject': quiz.subject})

    template_data = {
        'title': 'Manage Quizzes',
        'path': request.path.split('/')[:-1],
        'normal_quiz_data': normal_quiz_data,
        'canvas_quiz_data': canvas_quiz_data,
        'adaptive_quiz_data': adaptive_quiz_data,
        'previously_wrong_quiz_data': previously_wrong_quiz_data,
        'unseen_questions_quiz_data': unseen_questions_quiz_data
    }
    return render(request, 'teacher/manage_quiz/manage_quiz.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_modify_quiz_view(request):
    try:
        distribution_data = request.session['distribution_data']
        quiz_data = request.session['quiz_data']
        quiz_id = request.session['quiz_id']
    except KeyError:
        return redirect('teacher_manage_quizzes_view')

    if request.method == 'POST':
        if request.POST['quiz_type'] == 'normal':
            try:
                if request.POST['instruction'] == 'delete':
                    NormalQuiz.objects.filter(pk=int(request.POST['quiz_key'])).delete()
                elif request.POST['instruction'] == 'rename':
                    quiz = NormalQuiz.objects.get(pk=int(request.POST['quiz_key']))
                    quiz.name = request.POST['name']
                    quiz.save()
            except NormalQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'canvas':
            try:
                if request.POST['instruction'] == 'delete':
                    CanvasQuiz.objects.filter(pk=int(request.POST['quiz_key'])).delete()
                elif request.POST['instruction'] == 'rename':
                    quiz = CanvasQuiz.objects.get(pk=int(request.POST['quiz_key']))
                    quiz.name = request.POST['name']
                    quiz.save()
            except CanvasQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'adaptive':
            try:
                if request.POST['instruction'] == 'delete':
                    AdaptiveQuiz.objects.filter(pk=int(request.POST['quiz_key'])).delete()
                elif request.POST['instruction'] == 'rename':
                    quiz = AdaptiveQuiz.objects.get(pk=int(request.POST['quiz_key']))
                    quiz.name = request.POST['name']
                    quiz.save()
            except AdaptiveQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'previously_wrong':
            try:
                if request.POST['instruction'] == 'delete':
                    PreviouslyWrongQuiz.objects.filter(pk=int(request.POST['quiz_key'])).delete()
                elif request.POST['instruction'] == 'rename':
                    quiz = PreviouslyWrongQuiz.objects.get(pk=int(request.POST['quiz_key']))
                    quiz.name = request.POST['name']
                    quiz.save()
            except PreviouslyWrongQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')
        elif request.POST['quiz_type'] == 'unseen':
            try:
                if request.POST['instruction'] == 'delete':
                    UnseenQuestionsQuiz.objects.filter(pk=int(request.POST['quiz_key'])).delete()
                elif request.POST['instruction'] == 'rename':
                    quiz = UnseenQuestionsQuiz.objects.get(pk=int(request.POST['quiz_key']))
                    quiz.name = request.POST['name']
                    quiz.save()
            except UnseenQuestionsQuiz.DoesNotExist:
                return redirect('teacher_manage_quizzes_view')

    template_data = {
        'title': 'Manage Quiz',
        'path': request.path.split('/')[:-1],
        'quiz_data': quiz_data,
        'distribution_data': distribution_data,
        'quiz_id': quiz_id
    }
    return render(request, 'teacher/manage_quiz/modify_quiz.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_dashboard_view(request):
    template_data = {
        'title': 'Homepage',
        'path': request.path.split('/')[:-1]
    }
    return render(request, 'student/dashboard.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_confirm_quiz_view(request):
    try:
        distribution_data = request.session['distribution_data']
        quiz_data = request.session['quiz_data']
        quiz_type = quiz_data['Quiz Type']
        quiz_id = quiz_data['Quiz Id']

        if "Normal Quiz" == quiz_type:
            type_to_render = 'normal'
        elif "Adaptive Quiz" == quiz_type:
            type_to_render = 'adaptive'
        elif "Previously Wrong Quiz" == quiz_type:
            type_to_render = 'previously_wrong'
        elif "Unseen Questions Quiz" == quiz_type:
            type_to_render = 'unseen'
        elif "Canvas Quiz" == quiz_type:
            type_to_render = 'canvas'


    except KeyError:
        return redirect('student_quizzes_view')
    template_data = {
        'title': 'Confirm Quiz',
        'path': request.path.split('/')[:-1],
        'quiz_data': quiz_data,
        'distribution_data': distribution_data,
        'type_to_render': type_to_render,
        'quiz_id': quiz_id
    }
    return render(request, 'student/confirm_quiz.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_quizzes_view(request):
    try:
        del request.session['distribution_data']
        del request.session['quiz_data']
        del request.session['quiz_results']
    except KeyError:
        True

    if request.method == 'POST' and 'quiz_type' in request.POST and 'quiz_key' in request.POST:
        quiz_data = {}
        distribution_data = {}
        if request.POST['quiz_type'] == 'normal':
            try:
                quiz = NormalQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': 'Normal Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Difficulty': quiz.difficulty.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions,
                             'Quiz Id': int(request.POST['quiz_key'])
                             }
                distribution_pks = quiz.distributions.all().values_list('pk', flat=True)
                for pk in distribution_pks:
                    try:
                        distribution = QuizDistribution.objects.get(pk=pk)
                        distribution_data[distribution.category.name.title()] = distribution.percentage
                    except QuizDistribution.DoesNotExist:
                        return redirect('student_quizzes_view')
            except NormalQuiz.DoesNotExist:
                return redirect('student_quizzes_view')
        elif request.POST['quiz_type'] == 'canvas':
            try:
                quiz = CanvasQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_length = quiz.questions.all().values_list('pk', flat=True)
                quiz_data = {'Quiz Type': 'Canvas Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': len(quiz_length),
                             'Quiz Id': int(request.POST['quiz_key'])
                             }
            except CanvasQuiz.DoesNotExist:
                return redirect('student_quizzes_view')
        elif request.POST['quiz_type'] == 'adaptive':
            try:
                quiz = AdaptiveQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': 'Adaptive Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions,
                             'Quiz Id': int(request.POST['quiz_key'])
                             }
            except AdaptiveQuiz.DoesNotExist:
                return redirect('student_quizzes_view')
        elif request.POST['quiz_type'] == 'previously_wrong':
            try:
                quiz = PreviouslyWrongQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': 'Previously Wrong Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions,
                             'Quiz Id': int(request.POST['quiz_key'])
                             }
            except PreviouslyWrongQuiz.DoesNotExist:
                return redirect('student_quizzes_view')
        elif request.POST['quiz_type'] == 'unseen':
            try:
                quiz = UnseenQuestionsQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': 'Unseen Questions Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions,
                             'Quiz Id': int(request.POST['quiz_key'])
                             }
            except UnseenQuestionsQuiz.DoesNotExist:
                return redirect('student_quizzes_view')

        request.session['distribution_data'] = distribution_data
        request.session['quiz_data'] = quiz_data
        return redirect("student_confirm_quiz_view")

    normal_quiz_data = []
    canvas_quiz_data = []
    adaptive_quiz_data = []
    previously_wrong_quiz_data = []
    unseen_questions_quiz_data = []

    for pk in NormalQuiz.objects.filter().values_list('id', flat=True):
        quiz = NormalQuiz.objects.get(pk=pk)
        normal_quiz_data.append({'pk': quiz.pk,
                                 'name': quiz.name,
                                 'subject': quiz.subject})

    for pk in CanvasQuiz.objects.filter().values_list('id', flat=True):
        quiz = CanvasQuiz.objects.get(pk=pk)
        canvas_quiz_data.append({'pk': quiz.pk,
                                 'name': quiz.name,
                                 'subject': quiz.subject})

    for pk in AdaptiveQuiz.objects.filter().values_list('id', flat=True):
        quiz = AdaptiveQuiz.objects.get(pk=pk)
        adaptive_quiz_data.append({'pk': quiz.pk,
                                   'name': quiz.name,
                                   'subject': quiz.subject})

    for pk in PreviouslyWrongQuiz.objects.filter().values_list('id', flat=True):
        quiz = PreviouslyWrongQuiz.objects.get(pk=pk)
        previously_wrong_quiz_data.append({'pk': quiz.pk,
                                           'name': quiz.name,
                                           'subject': quiz.subject})

    for pk in UnseenQuestionsQuiz.objects.filter().values_list('id', flat=True):
        quiz = UnseenQuestionsQuiz.objects.get(pk=pk)
        unseen_questions_quiz_data.append({'pk': quiz.pk,
                                           'name': quiz.name,
                                           'subject': quiz.subject})

    template_data = {
        'title': 'Quizzes',
        'path': request.path.split('/')[:-1],
        'normal_quiz_data': normal_quiz_data,
        # 'canvas_quiz_data': canvas_quiz_data, #This is not used for this prototype
        'adaptive_quiz_data': adaptive_quiz_data,
        'previously_wrong_quiz_data': previously_wrong_quiz_data,
        'unseen_questions_quiz_data': unseen_questions_quiz_data
    }
    return render(request, 'student/view_quizzes.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_results_view(request):
    try:
        del request.session['distribution_data']
        del request.session['quiz_data']
        del request.session['quiz_results']
    except KeyError:
        True

    student_id = None

    if request.user.is_authenticated:
        student_id = request.user.pk

    if request.method == 'POST' and 'quiz_type' in request.POST and 'quiz_key' in request.POST:
        quiz_data = {}
        distribution_data = {}
        quiz_results = []
        if request.POST['quiz_type'] == 'normal':
            try:
                quiz = NormalQuiz.objects.get(pk=request.POST['quiz_key'])
                quiz_data = {'Quiz Type': 'Normal Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Difficulty': quiz.difficulty.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
                distribution_pks = quiz.distributions.all().values_list('pk', flat=True)
                for pk in distribution_pks:
                    distribution = QuizDistribution.objects.get(pk=pk)
                    distribution_data[distribution.category.name.title()] = distribution.percentage
                for pk in NormalQuizResult.objects.filter(quiz=quiz,
                                                          student_id=student_id).values_list('pk', flat=True):
                    result = NormalQuizResult.objects.get(pk=pk)
                    result_dict = {'pk': result.pk,
                                   'type': request.POST['quiz_type'],
                                   'score': '{0} / {1}'.format(result.score, quiz.no_of_questions),
                                   'percentage': round((result.score / quiz.no_of_questions) * 100),
                                   'date': result.date.strftime("%d-%m-%Y")}
                    quiz_results.append(result_dict)
                request.session['quiz_data'] = quiz_data
                request.session['distribution_data'] = distribution_data
                request.session['quiz_results'] = quiz_results
                redirect('students_attempts_view')
            except (NormalQuiz.DoesNotExist, NormalQuizResult.DoesNotExist, QuizDistribution.DoesNotExist) as e:
                return redirect('student_results_view')
        elif request.POST['quiz_type'] == 'canvas':
            try:
                quiz = CanvasQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_length = quiz.questions.all().values_list('pk', flat=True)
                quiz_data = {'Quiz Type': 'Canvas Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': len(quiz_length)
                             }
                for pk in CanvasQuizResult.objects.filter(quiz=quiz,
                                                          student_id=student_id).values_list('pk', flat=True):
                    result = CanvasQuizResult.objects.get(pk=pk)
                    result_dict = {'pk': result.pk,
                                   'type': request.POST['quiz_type'],
                                   'score': '{0} / {1}'.format(result.score, len(quiz_length)),
                                   'percentage': round((result.score / len(quiz_length)) * 100),
                                   'date': result.date.strftime("%d-%m-%Y")}
                    quiz_results.append(result_dict)
                request.session['quiz_data'] = quiz_data
                request.session['distribution_data'] = distribution_data
                request.session['quiz_results'] = quiz_results
                redirect('students_attempts_view')
            except (CanvasQuiz.DoesNotExist, CanvasQuizResult.DoesNotExist) as e:
                return redirect('student_results_view')
        elif request.POST['quiz_type'] == 'adaptive':
            try:
                quiz = AdaptiveQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': 'Adaptive Quiz',
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
                for pk in AdaptiveQuizResult.objects.filter(quiz=quiz,
                                                            student_id=student_id).values_list('pk', flat=True):
                    result = AdaptiveQuizResult.objects.get(pk=pk)
                    result_dict = {'pk': result.pk,
                                   'type': request.POST['quiz_type'],
                                   'score': '{0} / {1}'.format(result.score, quiz.no_of_questions),
                                   'percentage': round((result.score / quiz.no_of_questions) * 100),
                                   'date': result.date.strftime("%d-%m-%Y")}
                    quiz_results.append(result_dict)
                request.session['quiz_data'] = quiz_data
                request.session['distribution_data'] = distribution_data
                request.session['quiz_results'] = quiz_results
                redirect('students_attempts_view')
            except (AdaptiveQuiz.DoesNotExist, AdaptiveQuizResult.DoesNotExist) as e:
                return redirect('student_results_view')
        elif request.POST['quiz_type'] == 'previously_wrong':
            try:
                quiz = PreviouslyWrongQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': request.POST['quiz_type'],
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
                for pk in PreviouslyWrongQuizResult.objects.filter(quiz=quiz,
                                                                   student_id=student_id).values_list('pk', flat=True):
                    result = PreviouslyWrongQuizResult.objects.get(pk=pk)
                    result_dict = {'pk': result.pk,
                                   'type': request.POST['quiz_type'],
                                   'score': '{0} / {1}'.format(result.score, quiz.no_of_questions),
                                   'percentage': round((result.score / quiz.no_of_questions) * 100),
                                   'date': result.date.strftime("%d-%m-%Y")}
                    quiz_results.append(result_dict)
                request.session['quiz_data'] = quiz_data
                request.session['distribution_data'] = distribution_data
                request.session['quiz_results'] = quiz_results
                redirect('students_attempts_view')
            except (PreviouslyWrongQuiz.DoesNotExist, PreviouslyWrongQuizResult.DoesNotExist) as e:
                return redirect('student_results_view')
        elif request.POST['quiz_type'] == 'unseen':
            try:
                quiz = UnseenQuestionsQuiz.objects.get(pk=int(request.POST['quiz_key']))
                quiz_data = {'Quiz Type': request.POST['quiz_type'],
                             'Quiz Name': quiz.name,
                             'Subject': quiz.subject.name.title(),
                             'Time Limit (in minutes)': quiz.time_limit,
                             'Number of Questions': quiz.no_of_questions
                             }
                for pk in UnseenQuestionsQuizResult.objects.filter(quiz=quiz,
                                                                   student_id=student_id).values_list('pk', flat=True):
                    result = UnseenQuestionsQuizResult.objects.get(pk=pk)
                    result_dict = {'pk': result.pk,
                                   'type': request.POST['quiz_type'],
                                   'score': '{0} / {1}'.format(result.score, quiz.no_of_questions),
                                   'percentage': round((result.score / quiz.no_of_questions) * 100),
                                   'date': result.date.strftime("%d-%m-%Y")}
                    quiz_results.append(result_dict)
                request.session['quiz_data'] = quiz_data
                request.session['distribution_data'] = distribution_data
                request.session['quiz_results'] = quiz_results
                redirect('students_attempts_view')
            except (PreviouslyWrongQuiz.DoesNotExist, PreviouslyWrongQuizResult.DoesNotExist) as e:
                return redirect('student_results_view')

    normal_result_data = []
    canvas_result_data = []
    adaptive_result_data = []
    previously_wrong_result_data = []
    unseen_questions_result_data = []

    for pk in NormalQuiz.objects.filter().values_list('id', flat=True):
        quiz = NormalQuiz.objects.get(pk=pk)
        quiz_dict = {'pk': quiz.pk,
                     'name': quiz.name,
                     'subject': quiz.subject,
                     'attempts': 0,
                     'last_attempt': None}
        try:
            for pk in NormalQuizResult.objects.filter(quiz=quiz,
                                                      student_id=student_id).values_list('pk', flat=True):
                result_date = NormalQuizResult.objects.get(pk=pk).date
                quiz_dict['attempts'] += 1
                if quiz_dict['last_attempt'] is None or result_date > quiz_dict['last_attempt']:
                    quiz_dict['last_attempt'] = result_date
        except NormalQuizResult.DoesNotExist:
            continue
        if quiz_dict['attempts'] > 0:
            normal_result_data.append(quiz_dict)

    for pk in CanvasQuiz.objects.filter().values_list('id', flat=True):
        quiz = CanvasQuiz.objects.get(pk=pk)
        quiz_dict = {'pk': quiz.pk,
                     'name': quiz.name,
                     'subject': quiz.subject,
                     'attempts': 0,
                     'last_attempt': None}
        try:
            for pk in CanvasQuizResult.objects.filter(quiz=quiz,
                                                      student_id=student_id).values_list('pk', flat=True):
                result_date = CanvasQuizResult.objects.get(pk=pk).date
                quiz_dict['attempts'] += 1
                if quiz_dict['last_attempt'] is None or result_date > quiz_dict['last_attempt']:
                    quiz_dict['last_attempt'] = result_date
        except CanvasQuizResult.DoesNotExist:
            continue
        if quiz_dict['attempts'] > 0:
            canvas_result_data.append(quiz_dict)

    for pk in AdaptiveQuiz.objects.filter().values_list('id', flat=True):
        quiz = AdaptiveQuiz.objects.get(pk=pk)
        quiz_dict = {'pk': quiz.pk,
                     'name': quiz.name,
                     'subject': quiz.subject,
                     'attempts': 0,
                     'last_attempt': None}
        try:
            for pk in AdaptiveQuizResult.objects.filter(quiz=quiz,
                                                        student_id=student_id).values_list('pk', flat=True):
                result_date = AdaptiveQuizResult.objects.get(pk=pk).date
                quiz_dict['attempts'] += 1
                if quiz_dict['last_attempt'] is None or result_date > quiz_dict['last_attempt']:
                    quiz_dict['last_attempt'] = result_date
        except AdaptiveQuizResult.DoesNotExist:
            continue
        if quiz_dict['attempts'] > 0:
            adaptive_result_data.append(quiz_dict)

    for pk in PreviouslyWrongQuiz.objects.filter().values_list('id', flat=True):
        quiz = PreviouslyWrongQuiz.objects.get(pk=pk)
        quiz_dict = {'pk': quiz.pk,
                     'name': quiz.name,
                     'subject': quiz.subject,
                     'attempts': 0,
                     'last_attempt': None}
        try:
            for pk in PreviouslyWrongQuizResult.objects.filter(quiz=quiz,
                                                               student_id=student_id).values_list('pk', flat=True):
                result_date = PreviouslyWrongQuizResult.objects.get(pk=pk).date
                quiz_dict['attempts'] += 1
                if quiz_dict['last_attempt'] is None or result_date > quiz_dict['last_attempt']:
                    quiz_dict['last_attempt'] = result_date
        except PreviouslyWrongQuizResult.DoesNotExist:
            continue
        if quiz_dict['attempts'] > 0:
            previously_wrong_result_data.append(quiz_dict)

    for pk in UnseenQuestionsQuiz.objects.filter().values_list('id', flat=True):
        quiz = UnseenQuestionsQuiz.objects.get(pk=pk)
        quiz_dict = {'pk': quiz.pk,
                     'name': quiz.name,
                     'subject': quiz.subject,
                     'attempts': 0,
                     'last_attempt': None}
        try:
            for pk in UnseenQuestionsQuizResult.objects.filter(quiz=quiz,
                                                               student_id=student_id).values_list('pk', flat=True):
                result_date = UnseenQuestionsQuizResult.objects.get(pk=pk).date
                quiz_dict['attempts'] += 1
                if quiz_dict['last_attempt'] is None or result_date > quiz_dict['last_attempt']:
                    quiz_dict['last_attempt'] = result_date
        except UnseenQuestionsQuizResult.DoesNotExist:
            continue
        if quiz_dict['attempts'] > 0:
            unseen_questions_result_data.append(quiz_dict)

    template_data = {
        'title': 'Results',
        'path': request.path.split('/')[:-1],
        'normal_result_data': normal_result_data,
        'canvas_result_data': canvas_result_data,
        'adaptive_result_data': adaptive_result_data,
        'previously_wrong_result_data': previously_wrong_result_data,
        'unseen_questions_result_data': unseen_questions_result_data
    }
    return render(request, 'student/view_results.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def students_attempts_view(request):
    try:
        quiz_data = request.session['quiz_data']
        distribution_data = request.session['distribution_data']
        quiz_results = request.session['quiz_results']
    except KeyError:
        return redirect('student_results_view')

    student_id = None

    if request.user.is_authenticated:
        student_id = request.user.pk

    quiz_result = None

    print(request.session['quiz_data'])
    quiz_type = request.session['quiz_data']['Quiz Type']
    print(quiz_type)
    if "Canvas Quiz" == quiz_type:
        quiz_result = CanvasQuizResult.objects.filter(student_id=student_id)
    elif "Normal Quiz" == quiz_type:
        quiz_result = NormalQuizResult.objects.filter(student_id=student_id)
    elif "Adaptive Quiz" == quiz_type:
        quiz_result = AdaptiveQuizResult.objects.filter(student_id=student_id)
    elif "previously_wrong" == quiz_type:
        quiz_result = PreviouslyWrongQuizResult.objects.filter(student_id=student_id)
    elif "unseen" == quiz_type:
        quiz_result = UnseenQuestionsQuizResult.objects.filter(student_id=student_id)

    for i in quiz_result:
        if (i.quiz.name == request.session['quiz_data']['Quiz Name']):
            quiz_result = i
            break

    template_data = {
        'title': 'Attempts',
        'path': request.path.split('/')[:-1],
        'quiz_data': quiz_data,
        'distribution_data': distribution_data,
        'quiz_results': quiz_results[::-1],
        'quiz_result_object': quiz_result
    }
    request.session['quiz_data'] = quiz_data
    request.session['distribution_data'] = distribution_data
    request.session['quiz_results'] = quiz_results
    return render(request, 'student/view_attempts.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_normal_quiz_view(request, quiz_id):
    question_only = list(NormalQuiz.objects.get(pk=quiz_id).questions.all())[0]
    student_answers = []

    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]

    stimulus = Stimulus.objects.filter(questions=question_only)
    if len(stimulus) == 0:
        stimulus = Stimulus(text="", group_id=1)
    else:
        stimulus = stimulus[0]

    num_questions = NormalQuiz.objects.get(pk=quiz_id).no_of_questions
    time_limit = NormalQuiz.objects.get(pk=quiz_id).time_limit

    template_data = {
        "question": question,
        "results": False,
        "quiz_id": quiz_id,
        "student_answers": student_answers,
        "quiz_type": "normal_quiz",
        "stimulus": stimulus,
        "no_of_question": num_questions,
        "time_limit": time_limit
    }
    return render(request, 'student/quiz-page.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_adaptive_quiz_view(request, quiz_id):
    subject_id = AdaptiveQuiz.objects.get(pk=quiz_id).subject_id

    difficulty_id = -1

    for difficulty in difficulty_list:
        difficulty_object = Difficulty.objects.filter(subject_id=subject_id).get(name=difficulty)
        if difficulty_object.count > 0:
            difficulty_id = difficulty_object.pk
            break

    # There is no question for this subject
    if difficulty_id == -1:
        print("Uh-Oh no questions~")
        return

    index = randint(0, difficulty_object.count - 1)

    question_only = list(Question.objects.filter(difficulty_id=difficulty_id))[index]
    student_answers = []
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]
    stimulus = Stimulus.objects.filter(questions=question_only)
    if len(stimulus) == 0:
        stimulus = Stimulus(text="", group_id=1)
    else:
        stimulus = stimulus[0]

    score = 250

    num_questions = AdaptiveQuiz.objects.get(pk=quiz_id).no_of_questions
    time_limit = AdaptiveQuiz.objects.get(pk=quiz_id).time_limit

    template_data = {
        "question": question,
        "results": False,
        "quiz_id": quiz_id,
        "student_answers": student_answers,
        "quiz_type": "adaptive_quiz",
        "score": score,
        "stimulus": stimulus,
        "no_of_question": num_questions,
        "time_limit": time_limit,
        # Calculating points per question, for adaptive quiz only
        # 100: Point gap between 2 consequtive difficulties
        # 2: Arbitrary number
        # 6: # of difficulties with more than 0 questions.
        "points": (100 / (AdaptiveQuiz.objects.get(pk=quiz_id).no_of_questions / 2) / 6)
    }
    return render(request, 'student/quiz-page.html', template_data)


@permission_required('lti.is_student', raise_exception=True)
def student_unseen_quiz_view(request, quiz_id):
    user_id = 4

    if request.user.is_authenticated:
        user_id = request.user.pk

    user = User.objects.get(pk=user_id)
    all_questions = Question.objects.filter(subject=UnseenQuestionsQuiz.objects.get(pk=quiz_id).subject)
    seen_questions = Question.objects.filter(seen_users=user).filter(
        subject=UnseenQuestionsQuiz.objects.get(pk=quiz_id).subject)
    unseen_questions = [x for x in all_questions if x not in seen_questions]
    num_questions = UnseenQuestionsQuiz.objects.get(pk=quiz_id).no_of_questions
    index = randint(0, len(unseen_questions) - 1)
    question_only = list(unseen_questions)[index]

    student_answers = []
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]

    stimulus = Stimulus.objects.filter(questions=question_only)
    if len(stimulus) == 0:
        stimulus = Stimulus(text="", group_id=1)
    else:
        stimulus = stimulus[0]

    time_limit = UnseenQuestionsQuiz.objects.get(pk=quiz_id).time_limit

    template_data = {
        "question": question,
        "results": False,
        "quiz_id": quiz_id,
        "student_answers": student_answers,
        "quiz_type": "unseen_questions_quiz",
        "stimulus": stimulus,
        "no_of_question": num_questions,
        "time_limit": time_limit
    }
    return render(request, 'student/quiz-page.html', template_data)


def get_incorrect_quiz_question(quiz, incorrect_question_list):
    for i in quiz:
        for j in i.answers.all():
            correct_answer = Answer.objects.filter(question=j.question).get(weighting=100.0)
            selected_answer = j.selected_answer;

            if (correct_answer != selected_answer):
                incorrect_question_list.append(j.question)


@permission_required('lti.is_student', raise_exception=True)
def student_previously_wrong_quiz_view(request, quiz_id):
    user_id = 4

    if request.user.is_authenticated:
        user_id = request.user.pk

    user = User.objects.get(pk=user_id)
    all_questions = Question.objects.filter(subject=PreviouslyWrongQuiz.objects.get(pk=quiz_id).subject)

    normal_quiz_result_for_user = NormalQuizResult.objects.filter(student_id=user_id)
    adaptive_quiz_result_for_user = AdaptiveQuizResult.objects.filter(student_id=user_id)
    unseen_quiz_result_for_user = UnseenQuestionsQuizResult.objects.filter(student_id=user_id)
    prev_wrong_quiz_result_for_user = PreviouslyWrongQuizResult.objects.filter(student_id=user_id)

    incorrect_question_list = []

    get_incorrect_quiz_question(normal_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(adaptive_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(unseen_quiz_result_for_user, incorrect_question_list)
    get_incorrect_quiz_question(prev_wrong_quiz_result_for_user, incorrect_question_list)

    index = randint(0, len(incorrect_question_list) - 1)
    question_only = list(incorrect_question_list)[index]
    num_questions = PreviouslyWrongQuiz.objects.get(pk=quiz_id).no_of_questions

    student_answers = []
    shuffled_answers = (list(Answer.objects.filter(question=question_only)))
    random.shuffle(shuffled_answers)
    question = [question_only, shuffled_answers]

    stimulus = Stimulus.objects.filter(questions=question_only)
    if len(stimulus) == 0:
        stimulus = Stimulus(text="", group_id=1)
    else:
        stimulus = stimulus[0]

    time_limit = PreviouslyWrongQuiz.objects.get(pk=quiz_id).time_limit

    template_data = {
        "question": question,
        "results": False,
        "quiz_id": quiz_id,
        "student_answers": student_answers,
        "quiz_type": "prev_wrong_quiz",
        "stimulus": stimulus,
        "no_of_question": num_questions,
        "time_limit": time_limit
    }
    return render(request, 'student/quiz-page.html', template_data)


# TODO old stuff, delete later
@permission_required('lti.is_student', raise_exception=True)
def student_quiz_view(request, quiz_id=25):
    # store answers if POST
    if request.method == 'POST':
        question_list = []
        print(request.POST)
        for key in request.POST:
            if key.startswith("question_"):
                question = Question.objects.get(id=key[9:])
                answer = Answer.objects.get(id=request.POST[key])
                try:
                    stimulus = Stimulus.objects.get(questions=request.POST[key])
                except Stimulus.DoesNotExist:
                    stimulus = None

                question_answer = [question, answer, stimulus]
                question_list.append(question_answer)

        # should redirect instead of render to avoid double-rendering the form
        # but for now just render (for testing)
        template_data = {
            "question_list": question_list,
            "results": True
        }
        return render(request, 'student/quiz_new.html', template_data)

    else:
        # Obv wouldn't look like this in the final thing, this just makes a 'quiz' with the first 20 questions
        quiz_id = quiz_id
        print(list(NormalQuiz.objects.get(pk=quiz_id).questions.all()))
        question_only = list(NormalQuiz.objects.get(pk=quiz_id).questions.all())[0]
        student_answers = []
        question = [question_only, list(Answer.objects.filter(question=question_only))]
        quiz_object = NormalQuiz.objects.get(pk=quiz_id)
        no_of_question = quiz_object.no_of_questions
        time_limit = quiz_object.time_limit

        template_data = {
            "question": question,
            "results": False,
            "quiz_id": quiz_id,
            "student_answers": student_answers,
            "no_of_question": no_of_question,
            "time_limit": time_limit
        }
        return render(request, 'student/quiz-page.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def error404_test_teacher(request):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Not Found',
    }
    return render(request, 'teacher/error404.html', template_data, status=404)


@permission_required('lti.is_student', raise_exception=True)
def error404_test_student(request):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Not Found',
    }
    return render(request, 'student/error404.html', template_data, status=404)


def error404(request, exception):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Not Found',
    }
    if request.user.usertype.type == UserType.TEACHER:
        return render(request, 'teacher/error404.html', template_data, status=404)
    else:
        return render(request, 'student/error404.html', template_data, status=404)


@permission_required('lti.is_teacher', raise_exception=True)
def error403_test_teacher(request):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Forbidden',
    }
    return render(request, 'teacher/error403.html', template_data, status=403)


@permission_required('lti.is_student', raise_exception=True)
def error404_test_student(request):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Forbidden',
    }
    return render(request, 'student/error403.html', template_data, status=403)


def error403(request, exception):
    # This will only work with DEBUG = FALSE in settings.py
    template_data = {
        'title': 'Forbidden',
    }
    if request.user.usertype.type == UserType.TEACHER:
        return render(request, 'teacher/error403.html', template_data, status=403)
    else:
        return render(request, 'student/error403.html', template_data, status=403)
