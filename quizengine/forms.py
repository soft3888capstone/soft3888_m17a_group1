from django import forms

from quizconfig.models import NormalQuiz
from .models import Question
from .models import Subject


# gets a list of difficulties available
def get_difficulty_choices():
    try:
        added = []
        choices = []
        for question in Question.objects.all():
            if question.difficulty not in added:
                added.append(question.difficulty)
                choices.append((question.difficulty, question.difficulty.capitalize()))
        return choices
    except Exception:
        return []


class NormalQuizForm(forms.ModelForm):
    name = forms.CharField(
        max_length=120,
        label='Please enter a quiz name (this is viewable to students and will be how they identify the quiz)'
    )
    subject = forms.ModelChoiceField(
        label='Please select a quiz subject from the below options',
        queryset=Subject.objects.all(),
        to_field_name='name'
    )
    difficulty = forms.ChoiceField(
        label='Please enter a difficulty from the below options',
        widget=forms.Select,
        choices=get_difficulty_choices()
    )
    time_limit = forms.IntegerField(
        label='Please enter the time limit for the quiz (in minutes). Entering zero will make the quiz infinite. '
              'Maximum allowed is 120. '
    )
    no_of_questions = forms.IntegerField(
        label='Please enter the number of questions for the quiz (must be positive, greater than one).'
    )

    class Meta:
        model = NormalQuiz
        fields = [
            'name',
            'subject',
            'difficulty',
            'time_limit',
            'no_of_questions'
        ]
