from django.apps import AppConfig


class QuizEngineConfig(AppConfig):
    name = 'quizengine'
