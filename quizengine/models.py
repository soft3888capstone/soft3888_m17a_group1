from django.contrib.auth.models import User
from django.db import models


class Subject(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name.title()


class Difficulty(models.Model):
    name = models.CharField(max_length=120)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    count = models.IntegerField()

    def __str__(self):
        return self.name.title()


class QuestionCategory(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name.title()


class QuestionType(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name.title()


class Question(models.Model):
    seen_users = models.ManyToManyField(User)
    count_answered_correctly = models.IntegerField()
    count_answered_incorrectly = models.IntegerField()
    canvas_id = models.IntegerField()
    text = models.TextField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    difficulty = models.ForeignKey(Difficulty, on_delete=models.CASCADE)
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)  # complex operations
    type = models.ForeignKey(QuestionType, on_delete=models.CASCADE)  # multiple_choice_question
    comments = models.TextField(default="")


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()
    weighting = models.FloatField()


class Stimulus(models.Model):
    questions = models.ManyToManyField(Question)
    group_id = models.IntegerField()
    text = models.TextField()


class QuizDistribution(models.Model):
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)
    percentage = models.IntegerField()


class SubjectToCategory(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)
