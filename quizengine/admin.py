from django.contrib import admin

from .models import Answer
from .models import Difficulty
from .models import Question
from .models import QuestionCategory
from .models import QuestionType
from .models import QuizDistribution
from .models import Stimulus
from .models import Subject

admin.site.register(Subject)
admin.site.register(Difficulty)
admin.site.register(QuestionCategory)
admin.site.register(QuestionType)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Stimulus)
admin.site.register(QuizDistribution)
