//send question list here

var numberOFQuestions = 10;
var currentQuestion = 1;


function populateNumberOfQuestions(number) {
    numberOFQuestions = number;
}

function updateScreen() {

    var stimulus = document.getElementById('stimulus');
    //stimulus.innerHTML = 'Stimulus ' + currentQuestion;

    var questionCounter = document.getElementById('question-counter');
    questionCounter.innerHTML = currentQuestion;

}

function increment(amount) {
    if (amount > 0) {
        currentQuestion = currentQuestion + amount;

        if (currentQuestion > numberOFQuestions) {
            currentQuestion = numberOFQuestions;
        }

    }

    if (amount < 0) {
        currentQuestion = currentQuestion + amount;

        if (currentQuestion <= 1) {
            currentQuestion = 1;
        }
    }

    updateScreen();


}
