/* 
CODE USED/COPIED

https://github.com/akjasim/cb_dj_dependent_dropdown
https://www.youtube.com/watch?v=LmYDXgYK1so
*/

$("#id_subject").change(function () {
    const url = $("#normal_form").attr("data-difficulties-url");
    const subject_id = $(this).val();

    $.ajax({
        url: url,
        data: {
            'subject_id': subject_id
        },
        success: function (data) {
            $("#id_difficulty").html(data);
        }
    });
});
