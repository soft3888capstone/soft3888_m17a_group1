window.onscroll = function () {
    myFunction()
};

var header = document.getElementById("timer-header");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}


function getTimeLimit() {
    //Grab the interval value in minutes
    var interval;
    interval = parseInt(document.getElementById('timer-value-placeholder').innerText, 10);
    interval = interval * 60000; //minutes * 1min in ms
    return interval;
}

var interval = getTimeLimit();

// Set the date we're counting down to
var now = new Date().getTime();

var countDownDate = new Date(now + interval).getTime();

// Update the count down every 1 second
var x = setInterval(function () {
    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for hours, minutes and seconds
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    //padding the times
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    if (document.getElementById("timer-header").innerHTML != "Timer Done") {
        // Output the result in an element with id="demo"
        document.getElementById("timer-header").innerHTML = hours + " : "
            + minutes + " : " + seconds;
    }

    //If the count down is over, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer-header").innerHTML = "Timer Done";
    }
}, 1000);
