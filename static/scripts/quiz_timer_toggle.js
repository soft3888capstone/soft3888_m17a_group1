//This function is needed to get the state of the display
//A weird bug occurs if we query it directly using timer.style.display, where it returns nothing in the beginnign
function getStyle(id, name) {
    var element = document.getElementById(id);
    return element.currentStyle ? element.currentStyle[name] : window.getComputedStyle ? window.getComputedStyle(element, null).getPropertyValue(name) : null;
}


function toggleTimer() {
    // get the timer
    var timer = document.getElementById('timer-header');
    var timerDecorator = document.getElementById('timer-decorator');

    // get the current value of the timer's display property
    var displaySetting = getStyle('timer-header', 'display')

    // also get the timer button, so we can change what it says
    var timerButton = document.getElementById('show-hide-button');

    // now toggle the timer and the button text, depending on current state
    if (displaySetting == 'block') {
        // timer is visible. hide it
        timer.style.display = 'none';
        timerDecorator.style.display = 'none';
        // change button text
        timerButton.innerHTML = 'Show timer';
    } else {
        // timer is hidden. show it
        timer.style.display = 'block';
        timerDecorator.style.display = 'block';
        // change button text
        timerButton.innerHTML = 'Hide timer';
    }
}