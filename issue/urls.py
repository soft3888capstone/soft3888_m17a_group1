from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r'^student/issue/$', views.student_report_view, name='student_report_view'),
    re_path(r'^teacher/issue/$', views.teacher_report_view, name='teacher_report_view'),
]
