from django.db import models


class Issue(models.Model):
    email = models.EmailField(null=False)
    description = models.TextField(null=False)
    images = models.ImageField(upload_to='issue_screenshots/', default='issue_screenshots/null.png')
