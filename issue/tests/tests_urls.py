# VIDEO/CODE USED/COPIED FROM
# https://www.youtube.com/watch?v=0MrgsYswT1c

from django.test import TestCase
from django.urls import reverse, resolve

from issue.views import student_report_view
from issue.views import teacher_report_view


class TestUrls(TestCase):

    def test_student_report_url_resolves(self):
        url = reverse('student_report_view')
        self.assertEquals(resolve(url).func, student_report_view)

    def test_teacher_report_url_resolves(self):
        url = reverse('teacher_report_view')
        self.assertEquals(resolve(url).func, teacher_report_view)
