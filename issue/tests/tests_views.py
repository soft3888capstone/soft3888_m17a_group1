# VIDEO/CODE USED/COPIED FROM
# https://www.youtube.com/watch?v=hA_VxnxCHbo
# https://stackoverflow.com/questions/2473392/unit-testing-a-django-form-with-a-filefield


import os

from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from issue.forms import IssueForm
from issue.models import Issue


class TestStudentReportViews(TestCase):
    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def test_student_report_view_GET(self):
        client = self.login(self.user)

        url = reverse('student_report_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'student/base.html')
        self.assertTemplateUsed(response, 'student/report_issue.html')
        self.assertEquals(type(response.context['form']), IssueForm)

    def test_student_report_view_POST_no_image(self):
        client = self.login(self.user)

        url = reverse('student_report_view')
        form = {
            'email': 'testemail@fake.com',
            'description': 'My test gave me the wrong score.'
        }
        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'student/base.html')
        self.assertTemplateUsed(response, 'student/report_confirmation.html')
        try:
            Issue.objects.get(email=form['email'],
                              description=form['description'],
                              images='issue_screenshots/null.png')
        except Issue.DoesNotExist:
            self.fail()

    def test_student_report_view_POST_with_image(self):
        client = self.login(self.user)

        url = reverse('student_report_view')

        test_image_file = 'test98636596933559763686.png'
        original_test_path = os.path.join(os.getcwd(), 'issue', 'tests', test_image_file)
        issue_test_path = os.path.join(os.getcwd(), 'media', 'issue_screenshots', test_image_file)
        if os.path.exists(issue_test_path):
            os.remove(issue_test_path)
        upload_path = open(original_test_path,
                           'rb')  # See code used Jason Christa Unit Testing a Django Form with a FileField

        form = {
            'email': 'testemail@fake.com',
            'description': 'My test gave me the wrong score.',
            'images': SimpleUploadedFile(upload_path.name, upload_path.read())
            # See code used Jason Christa Unit Testing a Django Form with a FileField
        }
        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'student/base.html')
        self.assertTemplateUsed(response, 'student/report_confirmation.html')
        try:
            Issue.objects.get(email=form['email'],
                              description=form['description'],
                              images='issue_screenshots/' + test_image_file)
        except Issue.DoesNotExist:
            self.fail()


class TestTeacherReportViews(TestCase):
    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def test_teacher_report_view_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_report_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/report_issue.html')
        self.assertEquals(type(response.context['form']), IssueForm)
    
    def test_teacher_report_view_POST_no_image(self):
        client = self.login(self.user)

        url = reverse('teacher_report_view')
        form = {
            'email': 'testemail@fake.com',
            'description': 'My test gave me the wrong score.'
        }
        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/report_confirmation.html')
        try:
            Issue.objects.get(email=form['email'], 
                              description=form['description'],
                              images='issue_screenshots/null.png')
        except Issue.DoesNotExist:
            self.fail()

    def test_teacher_report_view_POST_with_image(self):
        client = self.login(self.user)

        url = reverse('teacher_report_view')

        test_image_file = 'test98636596933559763686.png'
        original_test_path = os.path.join(os.getcwd(), 'issue', 'tests', test_image_file)
        issue_test_path = os.path.join(os.getcwd(), 'media', 'issue_screenshots', test_image_file)
        if os.path.exists(issue_test_path):
            os.remove(issue_test_path)
        upload_path = open(original_test_path, 'rb')                            # See code used Jason Christa Unit Testing a Django Form with a FileField


        form = {
            'email': 'testemail@fake.com',
            'description': 'My test gave me the wrong score.',
            'images': SimpleUploadedFile(upload_path.name, upload_path.read())  # See code used Jason Christa Unit Testing a Django Form with a FileField
        }
        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/report_confirmation.html')
        try:
            Issue.objects.get(email=form['email'], 
                              description=form['description'],
                              images='issue_screenshots/'+test_image_file)
        except Issue.DoesNotExist:
            self.fail()
