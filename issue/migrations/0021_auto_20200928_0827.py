# Generated by Django 3.1.1 on 2020-09-28 08:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issue', '0020_auto_20200927_0503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='description',
            field=models.TextField(verbose_name='Please enter a description (what led to the issue, what were you doing at the time, etc)'),
        ),
        migrations.AlterField(
            model_name='issue',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Please enter a valid email (in-case we need to contact you further)'),
        ),
        migrations.AlterField(
            model_name='issue',
            name='images',
            field=models.ImageField(default='issue_screenshots/null.png', upload_to='issue_screenshots/', verbose_name='Add a relevant image (error messages, evidence, etc)'),
        ),
    ]
