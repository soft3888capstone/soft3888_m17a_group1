# Generated by Django 3.1.1 on 2020-09-26 17:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issue', '0002_auto_20200927_0323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='images',
            field=models.ImageField(default='default.jpg', upload_to=''),
        ),
    ]
