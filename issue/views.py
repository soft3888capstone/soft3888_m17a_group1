# CODE USED/COPIED FROM
# https://stackoverflow.com/questions/7669931/django-break-up-a-request-path-into-each-word

from django.contrib.auth.decorators import permission_required
from django.shortcuts import render

from .forms import IssueForm


@permission_required('lti.is_student', raise_exception=True)
def student_report_view(request):
    if request.method == 'POST':
        form = IssueForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # See code used break up a request.path into each word
            template_data = {
                'title': 'Issue Success',
                'path': request.path.split('/')[:-1]
            }
            return render(request, 'student/report_confirmation.html', template_data)

    form = IssueForm()
    template_data = {
        'title': 'Issue',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'student/report_issue.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_report_view(request):
    if request.method == 'POST':
        form = IssueForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            template_data = {
                'title': 'Issue Success',
                'path': request.path.split('/')[:-1]
            }
            return render(request, 'teacher/report_confirmation.html', template_data)

    form = IssueForm()
    template_data = {
        'title': 'Issue',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/report_issue.html', template_data)
