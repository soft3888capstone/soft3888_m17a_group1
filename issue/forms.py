from django import forms

from .models import Issue


class IssueForm(forms.ModelForm):
    email = forms.EmailField(label='Please enter a valid email (in-case we need to contact you further)')
    description = forms.CharField(widget=forms.Textarea,
                                  label='Please enter a description (what led to the issue, what were you doing at '
                                        'the time, etc)')
    images = forms.ImageField(label='Add a relevant image (error messages, evidence, etc)', required=False)

    class Meta:
        model = Issue
        fields = ['email',
                  'description',
                  'images']
