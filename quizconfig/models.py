from datetime import datetime

from django.db import models

from quizengine.models import Answer
from quizengine.models import Difficulty
from quizengine.models import Question
from quizengine.models import QuizDistribution
from quizengine.models import Subject


class QuizAnswers(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    selected_answer = models.ForeignKey(Answer, on_delete=models.CASCADE)


class NormalQuiz(models.Model):
    name = models.CharField(max_length=120)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    difficulty = models.ForeignKey(Difficulty, on_delete=models.CASCADE)
    distributions = models.ManyToManyField(QuizDistribution)
    time_limit = models.IntegerField()
    no_of_questions = models.IntegerField()
    questions = models.ManyToManyField(Question)


class NormalQuizResult(models.Model):
    quiz = models.ForeignKey(NormalQuiz, on_delete=models.CASCADE)
    student_id = models.IntegerField()
    date = models.DateField(default=datetime.now)
    score = models.IntegerField()
    answers = models.ManyToManyField(QuizAnswers)


class CanvasQuiz(models.Model):
    name = models.CharField(max_length=120)
    quiz_id = models.IntegerField()
    course_id = models.IntegerField()
    time_limit = models.IntegerField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    questions = models.ManyToManyField(Question)


class CanvasQuizResult(models.Model):
    quiz = models.ForeignKey(CanvasQuiz, on_delete=models.CASCADE)
    student_id = models.IntegerField()
    date = models.DateField(default=datetime.now)
    score = models.IntegerField()
    answers = models.ManyToManyField(QuizAnswers)


class AdaptiveQuiz(models.Model):
    name = models.CharField(max_length=120)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    time_limit = models.IntegerField()
    no_of_questions = models.IntegerField()


class AdaptiveQuizResult(models.Model):
    quiz = models.ForeignKey(AdaptiveQuiz, on_delete=models.CASCADE)
    student_id = models.IntegerField()
    date = models.DateField(default=datetime.now)
    score = models.IntegerField()
    answers = models.ManyToManyField(QuizAnswers)


class PreviouslyWrongQuiz(models.Model):
    name = models.CharField(max_length=120)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    time_limit = models.IntegerField()
    no_of_questions = models.IntegerField()


class PreviouslyWrongQuizResult(models.Model):
    quiz = models.ForeignKey(PreviouslyWrongQuiz, on_delete=models.CASCADE)
    student_id = models.IntegerField()
    date = models.DateField(default=datetime.now)
    score = models.IntegerField()
    answers = models.ManyToManyField(QuizAnswers)


class UnseenQuestionsQuiz(models.Model):
    name = models.CharField(max_length=120)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    time_limit = models.IntegerField()
    no_of_questions = models.IntegerField()


class UnseenQuestionsQuizResult(models.Model):
    quiz = models.ForeignKey(UnseenQuestionsQuiz, on_delete=models.CASCADE)
    student_id = models.IntegerField()
    date = models.DateField(default=datetime.now)
    score = models.IntegerField()
    answers = models.ManyToManyField(QuizAnswers)
