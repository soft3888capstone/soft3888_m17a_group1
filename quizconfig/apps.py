from django.apps import AppConfig


class QuizconfigConfig(AppConfig):
    name = 'quizconfig'
