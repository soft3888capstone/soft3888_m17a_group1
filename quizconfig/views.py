# CODE USED/COPIED FROM
# https://github.com/akjasim/cb_dj_dependent_dropdown
# https://www.youtube.com/watch?v=LmYDXgYK1so
# https://stackoverflow.com/questions/43851273/how-to-round-float-0-5-up-to-1-0-while-still-rounding-0-45-to-0-0-as-the-usual
# https://stackoverflow.com/questions/7669931/django-break-up-a-request-path-into-each-word

import math
import random

from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, redirect

import quizconfig.canvas_query_helper
from quizconfig.models import AdaptiveQuizResult
from quizconfig.models import CanvasQuiz
from quizconfig.models import CanvasQuizResult
from quizconfig.models import NormalQuiz
from quizconfig.models import NormalQuizResult
from quizconfig.models import PreviouslyWrongQuizResult
from quizconfig.models import UnseenQuestionsQuizResult
from quizengine.models import Difficulty
from quizengine.models import Question
from quizengine.models import QuestionCategory
from quizengine.models import QuizDistribution
from quizengine.models import Subject
from quizengine.models import SubjectToCategory
from .forms import AdaptiveQuizForm
from .forms import CanvasQuizForm
from .forms import DistributionQuizForm
from .forms import NormalQuizForm
from .forms import PreviouslyWrongQuizForm
from .forms import UnseenQuizForm


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_quiz_view(request):
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1]
    }
    # See code used break up a request.path into each word
    return render(request, 'teacher/create_quiz/create_quiz.html', template_data)


# See code used cb_dj_dependent_dropdown
@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_normal_quiz_view(request):
    if request.method == 'POST':
        form = NormalQuizForm(request.POST)
        errors = {}
        form_data = {'Quiz Type': 'Normal Quiz',
                     'Quiz Name': form['name'].value(),
                     'Subject': Subject.objects.get(pk=form['subject'].value()),
                     'Difficulty': Difficulty.objects.get(pk=form['difficulty'].value()),
                     'Time Limit (in minutes)': form['time_limit'].value(),
                     'Number of Questions': form['no_of_questions'].value()}

        if not form.is_valid():
            errors['Quiz Validity Error'] = 'A quiz must be valid (entering data in correct fashion).'
        if int(form['no_of_questions'].value()) <= 0:
            errors['Zero Questions Error'] = 'A quiz must have at least one question to be valid.'
        if int(form['no_of_questions'].value()) > Difficulty.objects.get(pk=form['difficulty'].value()).count:
            errors['Overflow Questions Error'] = 'Too many questions. {0} - {1} allows maximum {2} questions.'.format(
                Difficulty.objects.get(pk=form['difficulty'].value()).subject.name.title(),
                Difficulty.objects.get(pk=form['difficulty'].value()).name.title(),
                Difficulty.objects.get(pk=form['difficulty'].value()).count
            )
        if int(form['time_limit'].value()) < 0 or int(form['time_limit'].value()) > 120:
            errors['Time Error'] = 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'

        if len(errors.keys()) == 0:
            request.session['quiz_data'] = {'type': 'Normal Quiz',
                                            'name': form['name'].value(),
                                            'subject_pk': form['subject'].value(),
                                            'difficulty_pk': form['difficulty'].value(),
                                            'time_limit': form['time_limit'].value(),
                                            'no_of_questions': form['no_of_questions'].value()}
            request.session['distribution_percentage_data'] = {}
            request.session['distribution_count_data'] = {}
            return redirect('teacher_normal_distribution_view')
        else:
            template_data = {
                'title': 'Creation Failed',
                'path': request.path.split('/')[:-1],
                'errors': errors,
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/invalid_quiz.html', template_data)

    request.session['existing_distribution_data'] = {}
    form = NormalQuizForm()
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/create_quiz/normal_quiz.html', template_data)


# See code used cb_dj_dependent_dropdown
@permission_required('lti.is_teacher', raise_exception=True)
def load_difficulties(request):
    subject_id = request.GET.get('subject_id')
    difficulties = Difficulty.objects.filter(subject_id=subject_id).all().order_by('-count')
    return render(request, 'teacher/create_quiz/difficulty_dropdown_list_options.html', {'difficulties': difficulties})


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_normal_distribution_view(request):
    def get_categories(difficulty):
        return Question.objects.values_list('category').filter(difficulty=difficulty)

    def calculate_cumulative(distribution_precentage_data, excluded_category):
        cumulative = 0.0
        for k, v in distribution_precentage_data.items():
            if k.lower() != excluded_category.lower():
                cumulative += float(v)
        return cumulative

    # See code used rounding 0.5 up to 1.0
    def calculate_questions_count(percentage, quiz_length, number_of_questions_category):
        questions_count = (percentage / 100) * quiz_length

        if questions_count > number_of_questions_category:
            questions_count = number_of_questions_category
        elif (questions_count % 1) >= 0.5:
            questions_count = math.ceil(questions_count)
        else:
            questions_count = round(questions_count)

        percentage = round(((questions_count / quiz_length) * 100), 1)
        return percentage, questions_count

    def get_category_count(name, difficulty):
        category_count = {}
        for id in Question.objects.values_list('category').filter(difficulty=difficulty):
            if id[0] not in category_count.keys():
                category_count[id[0]] = 1
            else:
                category_count[id[0]] += 1
        return category_count[QuestionCategory.objects.get(name=name.lower()).pk]

    existing_quiz_data = request.session['quiz_data']
    errors = {}
    existing_distribution_percentage_data = request.session['distribution_percentage_data']
    existing_distribution_count_data = request.session['distribution_count_data']

    difficulty = Difficulty.objects.get(pk=existing_quiz_data['difficulty_pk'])

    for category in existing_distribution_percentage_data.keys():
        if request.method == 'POST' and 'delete_{0}'.format(category) in request.POST:
            existing_distribution_percentage_data.pop(category)
            existing_distribution_count_data.pop(category)
            request.session['distribution_percentage_data'] = existing_distribution_percentage_data
            request.session['distribution_count_data'] = existing_distribution_count_data
            break

    if request.method == 'POST' and 'quiz_submit' in request.POST:
        # Save distributions
        distribution_pks = []
        for key, value in existing_distribution_percentage_data.items():
            category = QuestionCategory.objects.get(name=key.lower())
            distribution = QuizDistribution(category=category,
                                            percentage=value)
            distribution.save()
            distribution_pks.append(distribution.pk)

        # Save quiz
        quiz = NormalQuiz(name=existing_quiz_data['name'],
                          subject=Subject.objects.get(pk=existing_quiz_data['subject_pk']),
                          difficulty=difficulty,
                          time_limit=existing_quiz_data['time_limit'],
                          no_of_questions=existing_quiz_data['no_of_questions'])
        quiz.save()
        quiz.distributions.set(distribution_pks)
        populate_normal_quiz_questions(quiz)

        form_data = {'Quiz Type': existing_quiz_data['type'],
                     'Quiz Name': quiz.name,
                     'Subject': quiz.subject,
                     'Difficulty': quiz.difficulty,
                     'Time Limit (in minutes)': quiz.time_limit,
                     'Number of Questions': quiz.no_of_questions}

        template_data = {
            'title': 'Creation Success',
            'path': request.path.split('/')[:-1],
            'form_data': form_data,
            'distribution_percentage_data': existing_distribution_percentage_data,
            'distribution_count_data': existing_distribution_count_data
        }
        return render(request, 'teacher/create_quiz/confirm_quiz.html', template_data)

    if request.method == 'POST' and 'category_submit' in request.POST:
        # Log percentage
        form = DistributionQuizForm(get_categories(difficulty), request.POST)

        cumulative = calculate_cumulative(existing_distribution_percentage_data, form['category'].value())
        percentage, count = calculate_questions_count(float(form['percentage'].value()),
                                                      int(existing_quiz_data['no_of_questions']),
                                                      get_category_count(form['category'].value(), difficulty))

        if cumulative >= 100.0:
            errors['Percentage Error'] = 'No more space. Please reduce or delete some category to continue.'
        elif float(form['percentage'].value()) <= 0 or float(form['percentage'].value()) > 100 - cumulative:
            errors['Percentage Error'] = 'A valid percentage must be greater than 0 and less than or equal to {0} (' \
                                         'cumulative).'.format(100 - cumulative)
        elif count == 0:
            errors['Percentage Error'] = 'Percentage is too small, reverted to zero therefore excluded. Please ' \
                                         'increase percentage or number of questions.'

        if len(errors.keys()) == 0:
            existing_distribution_percentage_data[form['category'].value()] = percentage
            existing_distribution_count_data[form['category'].value()] = count
            request.session['distribution_percentage_data'] = existing_distribution_percentage_data
            request.session['distribution_count_data'] = existing_distribution_count_data

    form = DistributionQuizForm(get_categories(difficulty))

    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form,
        'errors': errors,
        'distribution_percentage_data': existing_distribution_percentage_data,
        'distribution_count_data': existing_distribution_count_data
    }
    return render(request, 'teacher/create_quiz/distribution.html', template_data)


def populate_normal_quiz_questions(quiz):
    no_of_questions = int(quiz.no_of_questions)
    time_limit = quiz.time_limit
    difficulty = quiz.difficulty
    distributions = list(quiz.distributions.all())
    categories = []

    # Account for the case where user didn't select any distributions
    # In this case, select up to 3 categories from the DB at random and assign them random percentages.
    if len(distributions) == 0:
        subject_to_categories = list(SubjectToCategory.objects.filter(subject=quiz.subject))
        subject_categories = []

        for i in subject_to_categories:
            if len(Question.objects.filter(category=i.category).filter(difficulty=difficulty)) > no_of_questions / 3:
                subject_categories.append(i.category)

        for i in range(0, 3):
            # Select random category
            idx = random.randint(0, len(subject_categories) - 1)
            rand_cat = subject_categories[idx]
            subject_categories.pop(idx)
            distrib = QuizDistribution(category=rand_cat, percentage=random.randint(20, 33))
            distrib.save()
            quiz.distributions.add(distrib)
            distributions.append(distrib)

    for distribution in distributions:
        categories.append([distribution.category.name, distribution.percentage / 100.0])

    ret = []
    categs = []
    original_percentage = []
    q_ctr = 0

    # Preparing distributions
    for count, item in enumerate(categories):
        # Saving the original percentage for display purposes
        original_percentage.append(item[1] * 100)
        item[1] = math.floor(item[1] * no_of_questions)

        if (item[1] < 1):
            item[1] = 1

        q_ctr += item[1]

    # Adjust number of questions for each category until the sum of the questions = given no_of_questions
    for i in range(0, no_of_questions - q_ctr):
        categories[i % len(categories)][1] += 1

    questions = Question.objects.filter(difficulty=difficulty)

    for i in range(0, len(categories)):
        categs.append(list(filter(lambda x: x.category.name == categories[i][0], questions)))

    ceil = no_of_questions
    i = 0
    empty_categories = 0

    while i < ceil:
        if empty_categories >= len(categories):
            break

        curr_category = i % len(categories)
        if categories[curr_category][1] > 0:
            if (len(categs[curr_category]) - 1 < 0):
                next_q_idx = 0
                ceil += 1
                i += 1
                empty_categories += 1
                continue
            else:
                next_q_idx = random.randint(0, len(categs[curr_category]) - 1)
            ret.append(categs[curr_category][next_q_idx])
            categs[curr_category].pop(next_q_idx)
            categories[curr_category][1] -= 1
        else:
            ceil += 1
        i += 1

    for i in range(0, len(ret)):
        quiz.questions.add(ret[i])


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_canvas_example_view(request):
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1]
    }
    return render(request, 'teacher/create_quiz/canvas_example.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_canvas_quiz_view(request):
    if request.method == 'POST':
        form = CanvasQuizForm(request.POST)
        errors = {}
        form_data = {
            'Quiz Type': 'Canvas Quiz',
            'Quiz Name': form['name'].value(),
            'Canvas Course ID': form['course_id'].value(),
            'Canvas Quiz ID': form['quiz_id'].value()
        }

        canvas = quizconfig.canvas_query_helper.CanvasQueryHelper()
        form_course_id = int(form['course_id'].value())
        form_quiz_id = int(form['quiz_id'].value())
        subject = canvas.is_valid_course_id(form_course_id)

        if not form.is_valid():
            errors['Quiz Validity Error'] = 'A quiz must be valid (entering data in correct fashion).'
        if subject is None:
            errors['Canvas Course ID Error'] = 'The Canvas course ID must be a valid [Mathematics, General Ability, ' \
                                               'English] non revision course.'
        if subject is not None and not canvas.is_valid_quiz_id(form_course_id, form_quiz_id):
            errors['Canvas Quiz ID Error'] = 'The Canvas quiz ID must exist in the given course_id {0}.'.format(
                form_course_id)

        if len(errors.keys()) == 0:
            form_data['Subject'] = subject
            form_data['Time Limit (in minutes)'] = str(canvas.get_time_limit(form_course_id, form_quiz_id))
            canvas_quiz = CanvasQuiz(name=form['name'].value(),
                                     course_id=form_course_id,
                                     quiz_id=form_quiz_id,
                                     time_limit=canvas.get_time_limit(form_course_id, form_quiz_id),
                                     subject=subject)
            canvas_quiz.save()

            template_data = {
                'title': 'Creation Success',
                'path': request.path.split('/')[:-1],
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/confirm_quiz.html', template_data)
        else:
            template_data = {
                'title': 'Creation Failed',
                'path': request.path.split('/')[:-1],
                'errors': errors,
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/invalid_quiz.html', template_data)

    form = CanvasQuizForm()
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/create_quiz/canvas_quiz.html', template_data)


def get_max_questions(subject):
    return len(Question.objects.filter(subject=subject))


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_adaptive_quiz_view(request):
    if request.method == 'POST':
        form = AdaptiveQuizForm(request.POST)
        errors = {}
        form_data = {
            'Quiz Type': 'Adaptive Quiz',
            'Quiz Name': form['name'].value(),
            'Subject': Subject.objects.get(pk=form['subject'].value()),
            'Time Limit (in minutes)': form['time_limit'].value(),
            'Number of Questions': form['no_of_questions'].value()
        }

        subject = Subject.objects.get(pk=form['subject'].value())
        max_number_of_questions = len(Question.objects.filter(subject=subject))

        if not form.is_valid():
            errors['Quiz Validity Error'] = 'A quiz must be valid (entering data in correct fashion).'
        if int(form['no_of_questions'].value()) <= 0:
            errors['Zero Questions Error'] = 'A quiz must have at least one question to be valid.'
        if int(form['no_of_questions'].value()) > max_number_of_questions:
            errors['Overflow Questions Error'] = 'Too many questions. {0} allows maximum {1} questions.'.format(
                subject,
                max_number_of_questions
            )
        if int(form['time_limit'].value()) < 0 or int(form['time_limit'].value()) > 120:
            errors['Time Error'] = 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'

        if len(errors.keys()) == 0:
            form.save()
            template_data = {
                'title': 'Creation Success',
                'path': request.path.split('/')[:-1],
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/confirm_quiz.html', template_data)
        else:
            template_data = {
                'title': 'Creation Failed',
                'path': request.path.split('/')[:-1],
                'errors': errors,
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/invalid_quiz.html', template_data)

    form = AdaptiveQuizForm()
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/create_quiz/adaptive_quiz.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_previously_wrong_quiz_view(request):
    if request.method == 'POST':
        form = PreviouslyWrongQuizForm(request.POST)
        errors = {}
        form_data = {
            'Quiz Type': 'Previously Wrong Questions Quiz',
            'Quiz Name': form['name'].value(),
            'Subject': Subject.objects.get(pk=form['subject'].value()),
            'Time Limit (in minutes)': form['time_limit'].value(),
            'Number of Questions': form['no_of_questions'].value()
        }

        subject = Subject.objects.get(pk=form['subject'].value())
        max_number_of_questions = len(Question.objects.filter(subject=subject))

        if not form.is_valid():
            errors['Quiz Validity Error'] = 'A quiz must be valid (entering data in correct fashion).'
        if int(form['no_of_questions'].value()) <= 0:
            errors['Zero Questions Error'] = 'A quiz must have at least one question to be valid.'
        if int(form['no_of_questions'].value()) > max_number_of_questions:
            errors['Overflow Questions Error'] = 'Too many questions. {0} allows maximum {1} questions.'.format(
                subject,
                max_number_of_questions
            )
        if int(form['time_limit'].value()) < 0 or int(form['time_limit'].value()) > 120:
            errors['Time Error'] = 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'

        if len(errors.keys()) == 0:
            form.save()
            template_data = {
                'title': 'Creation Success',
                'path': request.path.split('/')[:-1],
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/confirm_quiz.html', template_data)
        else:
            template_data = {
                'title': 'Creation Failed',
                'path': request.path.split('/')[:-1],
                'errors': errors,
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/invalid_quiz.html', template_data)

    form = PreviouslyWrongQuizForm()
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/create_quiz/previously_wrong_quiz.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_create_unseen_quiz_view(request):
    if request.method == 'POST':
        form = UnseenQuizForm(request.POST)
        errors = {}
        form_data = {
            'Quiz Type': 'Unseen Questions Quiz',
            'Quiz Name': form['name'].value(),
            'Subject': Subject.objects.get(pk=form['subject'].value()),
            'Time Limit (in minutes)': form['time_limit'].value(),
            'Number of Questions': form['no_of_questions'].value()
        }

        subject = Subject.objects.get(pk=form['subject'].value())
        max_number_of_questions = len(Question.objects.filter(subject=subject))

        if not form.is_valid():
            errors['Quiz Validity Error'] = 'A quiz must be valid (entering data in correct fashion).'
        if int(form['no_of_questions'].value()) <= 0:
            errors['Zero Questions Error'] = 'A quiz must have at least one question to be valid.'
        if int(form['no_of_questions'].value()) > max_number_of_questions:
            errors['Overflow Questions Error'] = 'Too many questions. {0} allows maximum {1} questions.'.format(
                subject,
                max_number_of_questions
            )
        if int(form['time_limit'].value()) < 0 or int(form['time_limit'].value()) > 120:
            errors['Time Error'] = 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'

        if len(errors.keys()) == 0:
            form.save()
            template_data = {
                'title': 'Creation Success',
                'path': request.path.split('/')[:-1],
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/confirm_quiz.html', template_data)
        else:
            template_data = {
                'title': 'Creation Failed',
                'path': request.path.split('/')[:-1],
                'errors': errors,
                'form_data': form_data
            }
            return render(request, 'teacher/create_quiz/invalid_quiz.html', template_data)

    form = UnseenQuizForm()
    template_data = {
        'title': 'Create Quiz',
        'path': request.path.split('/')[:-1],
        'form': form
    }
    return render(request, 'teacher/create_quiz/unseen_quiz.html', template_data)


@permission_required('lti.is_teacher', raise_exception=True)
def teacher_show_results(request):
    student_id = None

    if request.method == 'GET':
        student_id = request.GET.get('search_bar', None)

    if student_id is None:
        normal_quiz_results = NormalQuizResult.objects.all()
        canvas_quiz_results = CanvasQuizResult.objects.all()
        adaptive_quiz_results = AdaptiveQuizResult.objects.all()
        previously_wrong_quiz_results = PreviouslyWrongQuizResult.objects.all()
        unseen_questions_quiz_results = UnseenQuestionsQuizResult.objects.all()

    else:
        normal_quiz_results = NormalQuizResult.objects.filter(student_id__contains=student_id)
        canvas_quiz_results = CanvasQuizResult.objects.filter(student_id__contains=student_id)
        adaptive_quiz_results = AdaptiveQuizResult.objects.filter(student_id__contains=student_id)
        previously_wrong_quiz_results = PreviouslyWrongQuizResult.objects.filter(student_id__contains=student_id)
        unseen_questions_quiz_results = UnseenQuestionsQuizResult.objects.filter(student_id__contains=student_id)

    context = {'normal_quiz_results': normal_quiz_results,
               'canvas_quiz_results': canvas_quiz_results,
               'adaptive_quiz_results': adaptive_quiz_results,
               'previously_wrong_quiz_results': previously_wrong_quiz_results,
               'unseen_questions_quiz_results': unseen_questions_quiz_results}

    return render(request, 'teacher/all_results.html', context)
