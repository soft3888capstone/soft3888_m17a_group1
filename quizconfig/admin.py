from django.contrib import admin

from .models import AdaptiveQuiz
from .models import AdaptiveQuizResult
from .models import CanvasQuiz
from .models import CanvasQuizResult
from .models import NormalQuiz
from .models import NormalQuizResult
from .models import PreviouslyWrongQuiz
from .models import PreviouslyWrongQuizResult
from .models import QuizAnswers
from .models import UnseenQuestionsQuiz
from .models import UnseenQuestionsQuizResult

admin.site.register(QuizAnswers)
admin.site.register(NormalQuiz)
admin.site.register(NormalQuizResult)
admin.site.register(CanvasQuiz)
admin.site.register(CanvasQuizResult)
admin.site.register(AdaptiveQuiz)
admin.site.register(AdaptiveQuizResult)
admin.site.register(PreviouslyWrongQuiz)
admin.site.register(PreviouslyWrongQuizResult)
admin.site.register(UnseenQuestionsQuiz)
admin.site.register(UnseenQuestionsQuizResult)
