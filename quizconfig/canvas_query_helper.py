import json

import requests
from canvasapi import Canvas

from quizengine.models import Subject


class CanvasQueryHelper:

    def __init__(self):
        API_URL = "https://300selective.instructure.com/"
        API_KEY = "13110~WKHvDb3G2WUFdEgvooCMatYyUYSo9qbxDXCE46hqT3ICPcVcJzVoDTziqDYzUeK6"
        self.HEADERS = {'Authorization': 'Bearer {0}'.format(API_KEY)}

        self.canvas = Canvas(API_URL, API_KEY)
        self.subjects = ['mathematics', 'english', 'general ability']

    # Determines if a given course_id is valid in the 300 selective Canvas
    def is_valid_course_id(self, course_id):
        for course in self.canvas.get_courses():
            for subject in self.subjects:
                if subject in course.name.lower() and 'revision' not in course.name.lower() and course.id == course_id:
                    return Subject.objects.get(name=subject.lower())
        return None

    # Determines if a given quiz_id is valid in the 300 selective Canvas
    def is_valid_quiz_id(self, course_id, quiz_id):
        course = self.canvas.get_course(course_id)
        for quiz in course.get_quizzes():
            if quiz.id == quiz_id:
                return True
        return False

    # Gets the time limit for a quiz extracted from Canvas
    def get_time_limit(self, course_id, quiz_id):
        url = 'https://300selective.instructure.com/api/v1/courses/{0}/quizzes/{1}'.format(
            course_id,
            quiz_id,
        )
        quiz = json.loads(requests.get(url, headers=self.HEADERS)._content)
        if quiz['time_limit'] is None:
            return 0
        else:
            return quiz['time_limit']
