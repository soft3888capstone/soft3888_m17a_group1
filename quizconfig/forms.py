# CODE USED/COPIED FROM
# https://github.com/akjasim/cb_dj_dependent_dropdown
# https://www.youtube.com/watch?v=LmYDXgYK1so

from django import forms

from quizengine.models import Difficulty
from quizengine.models import QuestionCategory
from quizengine.models import QuizDistribution
from .models import AdaptiveQuiz
from .models import CanvasQuiz
from .models import NormalQuiz
from .models import PreviouslyWrongQuiz
from .models import UnseenQuestionsQuiz


# See code used cb_dj_dependent_dropdown
class NormalQuizForm(forms.ModelForm):
    class Meta:
        model = NormalQuiz
        fields = ['name',
                  'subject',
                  'difficulty',
                  'time_limit',
                  'no_of_questions']

    def __init__(self, *args, **kwargs):
        super(NormalQuizForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Please enter a quiz name (this is viewable to students and will be how they ' \
                                    'identify the quiz) '
        self.fields['subject'].label = 'Please select a subject from the below options'
        self.fields['difficulty'].label = 'Please enter a difficulty from the below options'
        self.fields['time_limit'].label = 'Please enter the time limit for the quiz (in minutes). Entering zero will ' \
                                          'make the quiz infinite. Maximum allowed is 120 '
        self.fields['no_of_questions'].label = 'Please enter the number of questions for the quiz (Must be greater ' \
                                               'than one and less than number of questions available for difficulty) '
        self.fields['difficulty'].queryset = Difficulty.objects.none()

        if 'subject' in self.data:
            try:
                subject_id = int(self.data.get('subject'))
                self.fields['difficulty'].queryset = Difficulty.objects.filter(subject_id=subject_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['difficulty'].queryset = self.instance.subject.difficulty_set.order_by('name')


class DistributionQuizForm(forms.ModelForm):
    class Meta:
        model = QuizDistribution
        fields = ['category',
                  'percentage']

    def __init__(self, categories, *args, **kwargs):
        super(DistributionQuizForm, self).__init__(*args, **kwargs)

        count = {}
        for id in categories:
            if id[0] not in count.keys():
                count[id[0]] = 1
            else:
                count[id[0]] += 1

        categories_choices = []
        for k, v in count.items():
            categories_choices.append((QuestionCategory.objects.get(pk=k),
                                       '{0} ({1} Questions)'.format(QuestionCategory.objects.get(pk=k).name.title(),
                                                                    v)))

        self.fields['category'].label = 'Please select a category (selecting an already existing category will ' \
                                        'replace existing percentage) '
        self.fields['percentage'].label = 'Please enter percentage of questions you would like the quiz to be ' \
                                          'composed of for the selected category (cumulative percentage must be less ' \
                                          'than or equal to 100): '
        self.fields['category'].choices = categories_choices


class CanvasQuizForm(forms.ModelForm):
    class Meta:
        model = CanvasQuiz
        fields = ['name',
                  'course_id',
                  'quiz_id']

    def __init__(self, *args, **kwargs):
        super(CanvasQuizForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Please enter a quiz name (this is viewable to students and will be how they ' \
                                    'identify the quiz) '
        self.fields['course_id'].label = 'Please enter the Canvas course_id'
        self.fields['quiz_id'].label = 'Please enter the Canvas quiz_id'


class AdaptiveQuizForm(forms.ModelForm):
    class Meta:
        model = AdaptiveQuiz
        fields = ['name',
                  'subject',
                  'time_limit',
                  'no_of_questions']

    def __init__(self, *args, **kwargs):
        super(AdaptiveQuizForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Please enter a quiz name (this is viewable to students and will be how they ' \
                                    'identify the quiz) '
        self.fields['subject'].label = 'Please select a subject from the below options'
        self.fields['time_limit'].label = 'Please enter the time limit for the quiz (in minutes). Entering zero will ' \
                                          'make the quiz infinite. Maximum allowed is 120 '
        self.fields['no_of_questions'].label = 'Please enter the number of questions for the quiz (quiz may be ' \
                                               'reduced based on students history) '


class PreviouslyWrongQuizForm(forms.ModelForm):
    class Meta:
        model = PreviouslyWrongQuiz
        fields = ['name',
                  'subject',
                  'time_limit',
                  'no_of_questions']

    def __init__(self, *args, **kwargs):
        super(PreviouslyWrongQuizForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Please enter a quiz name (this is viewable to students and will be how they ' \
                                    'identify the quiz) '
        self.fields['subject'].label = 'Please select a subject from the below options'
        self.fields['time_limit'].label = 'Please enter the time limit for the quiz (in minutes). Entering zero will ' \
                                          'make the quiz infinite. Maximum allowed is 120 '
        self.fields['no_of_questions'].label = 'Please enter the number of questions for the quiz (quiz may be ' \
                                               'reduced based on students history) '


class UnseenQuizForm(forms.ModelForm):
    class Meta:
        model = UnseenQuestionsQuiz
        fields = ['name',
                  'subject',
                  'time_limit',
                  'no_of_questions']

    def __init__(self, *args, **kwargs):
        super(UnseenQuizForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = 'Please enter a quiz name (this is viewable to students and will be how they ' \
                                    'identify the quiz) '
        self.fields['subject'].label = 'Please select a subject from the below options'
        self.fields['time_limit'].label = 'Please enter the time limit for the quiz (in minutes). Entering zero will ' \
                                          'make the quiz infinite. Maximum allowed is 120 '
        self.fields['no_of_questions'].label = 'Please enter the number of questions for the quiz (quiz may be ' \
                                               'reduced based on students history) '
