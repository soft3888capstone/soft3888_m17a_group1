# Generated by Django 3.1.2 on 2020-11-07 04:12

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quizengine', '0001_initial'),
        ('quizconfig', '0003_canvasquiz_time_limit'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuizAnswers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizengine.question')),
                ('selected_answer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizengine.answer')),
            ],
        ),
        migrations.CreateModel(
            name='UnseenQuestionsQuizResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_id', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('score', models.IntegerField()),
                ('answers', models.ManyToManyField(to='quizconfig.QuizAnswers')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizconfig.unseenquestionsquiz')),
            ],
        ),
        migrations.CreateModel(
            name='PreviouslyWrongQuizResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_id', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('score', models.IntegerField()),
                ('answers', models.ManyToManyField(to='quizconfig.QuizAnswers')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizconfig.previouslywrongquiz')),
            ],
        ),
        migrations.CreateModel(
            name='NormalQuizResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_id', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('score', models.IntegerField()),
                ('answers', models.ManyToManyField(to='quizconfig.QuizAnswers')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizconfig.normalquiz')),
            ],
        ),
        migrations.CreateModel(
            name='CanvasQuizResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_id', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('score', models.IntegerField()),
                ('answers', models.ManyToManyField(to='quizconfig.QuizAnswers')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizconfig.canvasquiz')),
            ],
        ),
        migrations.CreateModel(
            name='AdaptiveQuizResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_id', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('score', models.IntegerField()),
                ('answers', models.ManyToManyField(to='quizconfig.QuizAnswers')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quizconfig.adaptivequiz')),
            ],
        ),
    ]
