''' 
CODE USED/COPIED

https://stackoverflow.com/questions/57740685/in-a-django-template-how-do-i-print-the-value-in-a-dictionary-when-the-key-is-s
'''
from django.template import Library

register = Library()


@register.filter
def lookup(d, key):
    return d.get(key)
