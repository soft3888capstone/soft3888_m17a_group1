# CODE USED/COPIED FROM
# https://github.com/akjasim/cb_dj_dependent_dropdown
# https://www.youtube.com/watch?v=LmYDXgYK1so


from django.urls import path
from django.urls import re_path

from . import views

urlpatterns = [

    path('teacher/dashboard/all_results/', views.teacher_show_results, name='teacher_all_results'),

    re_path(
        r'^teacher/dashboard/create_quiz/$',
        views.teacher_create_quiz_view,
        name='teacher_create_quiz'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/normal/$',
        views.teacher_create_normal_quiz_view,
        name='teacher_create_normal_quiz_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/adaptive/$',
        views.teacher_create_adaptive_quiz_view,
        name='teacher_create_adaptive_quiz_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/previously_wrong/$',
        views.teacher_create_previously_wrong_quiz_view,
        name='teacher_create_previously_wrong_quiz_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/unseen/$',
        views.teacher_create_unseen_quiz_view,
        name='teacher_create_unseen_quiz_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/load_difficulties/$',
        views.load_difficulties,
        name='ajax_load_difficulties'
    ),
    # See code used cb_dj_dependent_dropdown
    re_path(
        r'^teacher/dashboard/create_quiz/normal/distribution/$',
        views.teacher_normal_distribution_view,
        name='teacher_normal_distribution_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/canvas/$',
        views.teacher_create_canvas_quiz_view,
        name='teacher_create_canvas_quiz_view'
    ),
    re_path(
        r'^teacher/dashboard/create_quiz/canvas/example/$',
        views.teacher_canvas_example_view,
        name='teacher_canvas_example_view'
    ),
]
