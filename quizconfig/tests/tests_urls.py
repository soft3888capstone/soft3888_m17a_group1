# VIDEO/CODE USED/COPIED FROM
# https://www.youtube.com/watch?v=0MrgsYswT1c


from django.test import TestCase
from django.urls import reverse, resolve

from quizconfig.views import load_difficulties
from quizconfig.views import teacher_canvas_example_view
from quizconfig.views import teacher_create_adaptive_quiz_view
from quizconfig.views import teacher_create_canvas_quiz_view
from quizconfig.views import teacher_create_normal_quiz_view
from quizconfig.views import teacher_create_previously_wrong_quiz_view
from quizconfig.views import teacher_create_quiz_view
from quizconfig.views import teacher_create_unseen_quiz_view
from quizconfig.views import teacher_normal_distribution_view


class TestUrls(TestCase):

    def test_create_quiz_url_resolves(self):
        url = reverse('teacher_create_quiz')
        self.assertEquals(resolve(url).func, teacher_create_quiz_view)

    def test_create_quiz_normal_url_resolves(self):
        url = reverse('teacher_create_normal_quiz_view')
        self.assertEquals(resolve(url).func, teacher_create_normal_quiz_view)

    def test_teacher_normal_distribution_url_resolves(self):
        url = reverse('teacher_normal_distribution_view')
        self.assertEquals(resolve(url).func, teacher_normal_distribution_view)

    def test_load_difficulties_resolves(self):
        url = reverse('ajax_load_difficulties')
        self.assertEquals(resolve(url).func, load_difficulties)

    def test_create_quiz_adaptive_url_resolves(self):
        url = reverse('teacher_create_adaptive_quiz_view')
        self.assertEquals(resolve(url).func, teacher_create_adaptive_quiz_view)

    def test_create_quiz_previously_wrong_url_resolves(self):
        url = reverse('teacher_create_previously_wrong_quiz_view')
        self.assertEquals(resolve(url).func, teacher_create_previously_wrong_quiz_view)

    def test_create_quiz_unseen_url_resolves(self):
        url = reverse('teacher_create_unseen_quiz_view')
        self.assertEquals(resolve(url).func, teacher_create_unseen_quiz_view)

    def test_create_quiz_canvas_url_resolves(self):
        url = reverse('teacher_create_canvas_quiz_view')
        self.assertEquals(resolve(url).func, teacher_create_canvas_quiz_view)

    def test_create_quiz_canvas_example_url_resolves(self):
        url = reverse('teacher_canvas_example_view')
        self.assertEquals(resolve(url).func, teacher_canvas_example_view)
