# VIDEO/CODE USED/COPIED FROM
# https://www.youtube.com/watch?v=hA_VxnxCHbo

import sys

from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from quizconfig.forms import AdaptiveQuizForm
from quizconfig.forms import CanvasQuizForm
from quizconfig.forms import DistributionQuizForm
from quizconfig.forms import NormalQuizForm
from quizconfig.forms import PreviouslyWrongQuizForm
from quizconfig.forms import UnseenQuizForm
from quizconfig.models import AdaptiveQuiz
from quizconfig.models import CanvasQuiz
from quizconfig.models import NormalQuiz
from quizconfig.models import PreviouslyWrongQuiz
from quizconfig.models import UnseenQuestionsQuiz
from quizengine.models import Difficulty
from quizengine.models import Question
from quizengine.models import QuestionCategory
from quizengine.models import QuestionType
from quizengine.models import QuizDistribution
from quizengine.models import Subject


class TestCreateQuizViews(TestCase):
    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def test_create_quiz_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_quiz')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/create_quiz.html')


class TestNormalQuizViews(TestCase):

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

        self.subject = Subject(name='english')
        self.subject.save()
        self.difficulty = Difficulty(name='hard',
                                     subject=self.subject,
                                     count=20)
        self.difficulty.save()

    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def test_create_quiz_normal_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/normal_quiz.html')
        self.assertEquals(type(response.context['form']), NormalQuizForm)

    def test_create_quiz_normal_POST(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': 30,
            'no_of_questions': 15
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/teacher/dashboard/create_quiz/normal/distribution/')
        self.assertEquals(session['quiz_data'], {'type': 'Normal Quiz',
                                                 'name': form['name'],
                                                 'subject_pk': str(form['subject']),
                                                 'difficulty_pk': str(form['difficulty']),
                                                 'time_limit': str(form['time_limit']),
                                                 'no_of_questions': str(form['no_of_questions'])})
        self.assertEquals(len(session['distribution_percentage_data']), 0)
        self.assertEquals(len(session['distribution_count_data']), 0)

    def test_create_quiz_normal_POST_error_zero_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': 30,
            'no_of_questions': 0
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_create_quiz_normal_POST_error_negative_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': 30,
            'no_of_questions': -sys.maxsize - 1
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_create_quiz_normal_POST_error_overflow_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': 30,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Overflow Questions Error': 'Too many questions. English - Hard allows maximum 20 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_create_quiz_normal_POST_error_time_limit_max(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': sys.maxsize,
            'no_of_questions': 15
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_create_quiz_normal_POST_error_time_limit_min(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': 15
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_create_quiz_normal_POST_error_multiple(self):
        client = self.login(self.user)

        url = reverse('teacher_create_normal_quiz_view')
        form = {
            'name': 'Hard English Quiz Test',
            'subject': self.subject.pk,
            'difficulty': self.difficulty.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': 0
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.',
                           'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})

    def test_load_difficulties(self):
        client = self.login(self.user)

        url = reverse('ajax_load_difficulties')

        # Add subjects
        test_subject = None
        subjects = ['english', 'mathematics']
        for subject in subjects:
            temp = Subject(name=subject)
            temp.save()
            if subject == 'mathematics':
                test_subject = temp

        # Add difficulties
        difficulties = {'easy': 3, 'medium': 2, 'hard': 4}
        for k, v in difficulties.items():
            db_difficulty = Difficulty(name=k, subject=test_subject, count=v)
            db_difficulty.save()

        response = client.get(url, {'subject_id': test_subject.pk})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/create_quiz/difficulty_dropdown_list_options.html')
        self.assertTrue(list(response.context['difficulties']) == list(
            Difficulty.objects.filter(subject_id=test_subject.pk).all().order_by('-count')))

    def test_load_difficulties_empty(self):
        client = self.login(self.user)

        url = reverse('ajax_load_difficulties')

        # Add subjects
        test_subject = Subject(name='english')
        test_subject.save()

        response = client.get(url, {'subject_id': test_subject.pk})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/create_quiz/difficulty_dropdown_list_options.html')
        self.assertEquals(response.context['difficulties'].count(), 0)


class TestNormalDistributionViews(TestCase):
    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)

        return client

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()
        '''
        Mathematics
        Easy
        Multiple Choice
            Geometry - 5 Questions
            Algebra - 5 Questions
            Graphs - 5 Questions
            Calculus - 5 Questions
        '''
        self.subject = Subject(name='mathematics')
        self.subject.save()
        self.difficulty = Difficulty(name='easy',
                                     subject=self.subject,
                                     count=0)
        self.difficulty.save()
        type = QuestionType(name='multiple_choice')
        type.save()
        self.categories = ['Geometry', 'Algebra', 'Graphs', 'Calculus']
        for name in self.categories:
            category = QuestionCategory(name=name.lower())
            category.save()
            for x in range(0, 5):
                question = Question(count_answered_correctly=0,
                                    count_answered_incorrectly=0,
                                    canvas_id=x,
                                    text='dummy question{0}'.format(x),
                                    subject=self.subject,
                                    difficulty=self.difficulty,
                                    category=category,
                                    type=type)
                question.save()
        client = self.login(self.user)

    def test_teacher_normal_distribution_GET(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        session = client.session
        session['distribution_percentage_data'] = {}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        session = client.session
        session['distribution_count_data'] = {}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(len(response.context['errors']), 0)
        self.assertEquals(len(response.context['distribution_percentage_data']), 0)
        self.assertEquals(len(response.context['distribution_count_data']), 0)

    def test_teacher_normal_distribution_POST_delete(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 40.0, self.categories[2]: 20.0}
        session['distribution_count_data'] = {self.categories[0]: 4, self.categories[2]: 2}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        response = client.post(url, {'delete_{0}'.format(self.categories[0]): 'submit'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(len(response.context['errors']), 0)
        self.assertEquals(response.context['distribution_percentage_data'], {self.categories[2]: 20.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[2]: 2})

    def test_teacher_normal_distribution_POST_save(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 40.0, self.categories[2]: 20.0}
        session['distribution_count_data'] = {self.categories[0]: 4, self.categories[2]: 2}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        response = client.post(url, {'quiz_submit': 'submit'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/confirm_quiz.html')
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Normal Quiz',
                                                          'Quiz Name': 'Easy Maths Quiz Test',
                                                          'Subject': self.subject,
                                                          'Difficulty': self.difficulty,
                                                          'Time Limit (in minutes)': 30,
                                                          'Number of Questions': 10})
        self.assertEquals(response.context['distribution_percentage_data'],
                          {self.categories[0]: 40.0, self.categories[2]: 20.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[0]: 4, self.categories[2]: 2})
        distribution_pks = []

        for k, v in response.context['distribution_percentage_data'].items():
            try:
                category = QuestionCategory.objects.get(name=k.lower())
                try:
                    distribution = QuizDistribution.objects.get(category=category, percentage=v)
                    distribution_pks.append(distribution.pk)
                except QuizDistribution.DoesNotExist:
                    self.fail()
            except QuestionCategory.DoesNotExist:
                self.fail()
        try:
            normal_quiz = NormalQuiz.objects.get(name='Easy Maths Quiz Test',
                                                 subject=self.subject,
                                                 difficulty=self.difficulty,
                                                 time_limit=30,
                                                 no_of_questions=10)
            self.assertEquals(list(normal_quiz.distributions.all().values_list('pk', flat=True)), distribution_pks)
        except AdaptiveQuiz.DoesNotExist:
            self.fail()

    def test_teacher_normal_distribution_POST_add_category_new(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 40.0, self.categories[2]: 20.0}
        session['distribution_count_data'] = {self.categories[0]: 4, self.categories[2]: 2}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        form = {
            'category': QuestionCategory.objects.get(name=self.categories[1].lower()),
            'percentage': 20,
            'category_submit': 'submit'
        }

        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(len(response.context['errors']), 0)
        self.assertEquals(response.context['distribution_percentage_data'],
                          {self.categories[0]: 40.0, self.categories[2]: 20.0, self.categories[1]: 20.0})
        self.assertEquals(response.context['distribution_count_data'],
                          {self.categories[0]: 4, self.categories[2]: 2, self.categories[1]: 2})

    def test_teacher_normal_distribution_POST_add_category_overwrite(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 40.0, self.categories[2]: 20.0}
        session['distribution_count_data'] = {self.categories[0]: 4, self.categories[2]: 2}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        form = {
            'category': QuestionCategory.objects.get(name=self.categories[0].lower()),
            'percentage': 20,
            'category_submit': 'submit'
        }

        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(len(response.context['errors']), 0)
        self.assertEquals(response.context['distribution_percentage_data'],
                          {self.categories[0]: 20.0, self.categories[2]: 20.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[0]: 2, self.categories[2]: 2})

    def test_teacher_normal_distribution_POST_error_full(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 50.0, self.categories[2]: 50.0}
        session['distribution_count_data'] = {self.categories[0]: 5, self.categories[2]: 5}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        form = {
            'category': QuestionCategory.objects.get(name=self.categories[1].lower()),
            'percentage': 20,
            'category_submit': 'submit'
        }

        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(response.context['errors'],
                          {'Percentage Error': 'No more space. Please reduce or delete some category to continue.'})
        self.assertEquals(response.context['distribution_percentage_data'],
                          {self.categories[0]: 50.0, self.categories[2]: 50.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[0]: 5, self.categories[2]: 5})

    def test_teacher_normal_distribution_POST_error_too_large(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 50.0}
        session['distribution_count_data'] = {self.categories[0]: 5}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        form = {
            'category': QuestionCategory.objects.get(name=self.categories[1].lower()),
            'percentage': 75,
            'category_submit': 'submit'
        }

        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(response.context['errors'], {
            'Percentage Error': 'A valid percentage must be greater than 0 and less than or equal to 50.0 (cumulative).'})
        self.assertEquals(response.context['distribution_percentage_data'], {self.categories[0]: 50.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[0]: 5})

    def test_teacher_normal_distribution_POST_error_too_small(self):
        client = self.login(self.user)

        session = client.session
        session['quiz_data'] = {'type': 'Normal Quiz',
                                'name': 'Easy Maths Quiz Test',
                                'subject_pk': self.subject.pk,
                                'difficulty_pk': self.difficulty.pk,
                                'time_limit': 30,
                                'no_of_questions': 10}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        url = reverse('teacher_normal_distribution_view')

        session['distribution_percentage_data'] = {self.categories[0]: 50.0}
        session['distribution_count_data'] = {self.categories[0]: 5}
        session.save()
        session_cookie_name = settings.SESSION_COOKIE_NAME
        client.cookies[session_cookie_name] = session.session_key

        form = {
            'category': QuestionCategory.objects.get(name=self.categories[1].lower()),
            'percentage': 1,
            'category_submit': 'submit'
        }

        response = client.post(url, form)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/distribution.html')
        self.assertEquals(type(response.context['form']), DistributionQuizForm)
        self.assertEquals(response.context['errors'], {
            'Percentage Error': 'Percentage is too small, reverted to zero therefore excluded. Please increase percentage or number of questions.'})
        self.assertEquals(response.context['distribution_percentage_data'], {self.categories[0]: 50.0})
        self.assertEquals(response.context['distribution_count_data'], {self.categories[0]: 5})


class TestAdaptiveQuizViews(TestCase):
    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

        self.subject = Subject(name='english')
        self.subject.save()
        difficulty = Difficulty(name='easy',
                                subject=self.subject,
                                count=10)
        difficulty.save()
        category = QuestionCategory(name='poetry')
        category.save()
        type = QuestionType(name='multiple_choice')
        type.save()
        for x in range(0, 10):  # 10 questions for this subject
            question = Question(
                count_answered_correctly=0,
                count_answered_incorrectly=0,
                canvas_id=x,
                text='dummy question{0}'.format(x),
                subject=self.subject,
                difficulty=difficulty,
                category=category,
                type=type)
            question.save()

    def test_create_quiz_adaptive_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/adaptive_quiz.html')
        self.assertEquals(type(response.context['form']), AdaptiveQuizForm)

    def test_create_quiz_adaptive_POST(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 30,
            'no_of_questions': 5
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/confirm_quiz.html')
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
        except AdaptiveQuiz.DoesNotExist:
            self.fail()

    def test_create_quiz_adaptive_POST_error_zero_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 30,
            'no_of_questions': 0
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_adaptive_POST_error_negative_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 30,
            'no_of_questions': -sys.maxsize - 1
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_adaptive_POST_error_overflow_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 30,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Overflow Questions Error': 'Too many questions. English allows maximum 10 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_adaptive_POST_error_time_limit_max(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': sys.maxsize,
            'no_of_questions': 5
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_adaptive_POST_error_time_limit_min(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': 5
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_adaptive_POST_error_multiple(self):
        client = self.login(self.user)

        url = reverse('teacher_create_adaptive_quiz_view')
        form = {
            'name': 'English Adaptive Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.',
            'Overflow Questions Error': 'Too many questions. English allows maximum 10 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Adaptive Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            AdaptiveQuiz.objects.get(name=form['name'],
                                     subject=self.subject,
                                     time_limit=form['time_limit'],
                                     no_of_questions=form['no_of_questions'])
            self.fail()
        except AdaptiveQuiz.DoesNotExist:
            # Pass test case does not exist
            True


class TestPreviouslyWrongViews(TestCase):
    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

        self.subject = Subject(name='mathematics')
        self.subject.save()
        difficulty = Difficulty(name='medium',
                                subject=self.subject,
                                count=14)
        difficulty.save()
        category = QuestionCategory(name='algebra')
        category.save()
        type = QuestionType(name='multiple_choice')
        type.save()
        for x in range(0, 14):  # 14 questions for this subject
            question = Question(
                count_answered_correctly=0,
                count_answered_incorrectly=0,
                canvas_id=x,
                text='dummy question{0}'.format(x),
                subject=self.subject,
                difficulty=difficulty,
                category=category,
                type=type)
            question.save()

    def test_create_quiz_previously_wrong_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/previously_wrong_quiz.html')
        self.assertEquals(type(response.context['form']), PreviouslyWrongQuizForm)

    def test_create_quiz_previously_wrong_POST(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 60,
            'no_of_questions': 12
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/confirm_quiz.html')
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
        except PreviouslyWrongQuiz.DoesNotExist:
            self.fail()

    def test_create_quiz_previously_wrong_POST_error_zero_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 60,
            'no_of_questions': 0
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_previously_wrong_POST_error_negative_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 60,
            'no_of_questions': -sys.maxsize - 1
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_previously_wrong_POST_error_overflow_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 60,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Overflow Questions Error': 'Too many questions. Mathematics allows maximum 14 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_previously_wrong_POST_error_time_limit_max(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': sys.maxsize,
            'no_of_questions': 12
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_previously_wrong_POST_error_time_limit_min(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': 12
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_previously_wrong_POST_error_multiple(self):
        client = self.login(self.user)

        url = reverse('teacher_create_previously_wrong_quiz_view')
        form = {
            'name': 'Maths Previously Wrong Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.',
            'Overflow Questions Error': 'Too many questions. Mathematics allows maximum 14 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Previously Wrong Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            PreviouslyWrongQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except PreviouslyWrongQuiz.DoesNotExist:
            # Pass test case does not exist
            True


class TestUnseenQuizViews(TestCase):
    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

        self.subject = Subject(name='general ability')
        self.subject.save()
        difficulty = Difficulty(name='challenge',
                                subject=self.subject,
                                count=8)
        difficulty.save()
        category = QuestionCategory(name='reading')
        category.save()
        type = QuestionType(name='multiple_choice')
        type.save()
        for x in range(0, 8):  # 8 questions for this subject
            question = Question(
                count_answered_correctly=0,
                count_answered_incorrectly=0,
                canvas_id=x,
                text='dummy question{0}'.format(x),
                subject=self.subject,
                difficulty=difficulty,
                category=category,
                type=type)
            question.save()

    def test_create_quiz_unseen_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/unseen_quiz.html')
        self.assertEquals(type(response.context['form']), UnseenQuizForm)

    def test_create_quiz_unseen_POST(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 45,
            'no_of_questions': 6
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/confirm_quiz.html')
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
        except UnseenQuestionsQuiz.DoesNotExist:
            self.fail()

    def test_create_quiz_unseen_POST_error_zero_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 45,
            'no_of_questions': 0
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_unseen_POST_error_negative_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 45,
            'no_of_questions': -sys.maxsize - 1
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Zero Questions Error': 'A quiz must have at least one question to be valid.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_unseen_POST_error_overflow_questions(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': 45,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Overflow Questions Error': 'Too many questions. General Ability allows maximum 8 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_unseen_POST_error_time_limit_max(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': sys.maxsize,
            'no_of_questions': 6
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_unseen_POST_error_time_limit_min(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': 6
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_unseen_POST_error_multiple(self):
        client = self.login(self.user)

        url = reverse('teacher_create_unseen_quiz_view')
        form = {
            'name': 'General Ability Unseen Quiz Test',
            'subject': self.subject.pk,
            'time_limit': -sys.maxsize - 1,
            'no_of_questions': sys.maxsize
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Time Error': 'A quiz has a minimum time of 0 (infinite) and a maximum time limit of 120 minutes.',
            'Overflow Questions Error': 'Too many questions. General Ability allows maximum 8 questions.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Unseen Questions Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': str(form['time_limit']),
                                                          'Number of Questions': str(form['no_of_questions'])})
        try:
            UnseenQuestionsQuiz.objects.get(name=form['name'],
                                            subject=self.subject,
                                            time_limit=form['time_limit'],
                                            no_of_questions=form['no_of_questions'])
            self.fail()
        except UnseenQuestionsQuiz.DoesNotExist:
            # Pass test case does not exist
            True


class TestCanvasQuizViews(TestCase):
    def login(self, user):
        client = Client()
        result = client.login(username=user.username, password=self.test_password)
        self.assertEqual(result, True)
        return client

    def setUp(self):
        cache.clear()
        self.test_password = 'admin'
        self.user = User.objects.create_user(username='admin', email='admin@admin.com', is_superuser=True)
        self.user.set_password(self.test_password)
        self.user.save()

        self.subject = Subject(name='english')
        self.subject.save()

    def test_create_quiz_canvas_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_create_canvas_quiz_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/canvas_quiz.html')
        self.assertEquals(type(response.context['form']), CanvasQuizForm)

    def test_create_quiz_canvas_POST(self):
        client = self.login(self.user)

        url = reverse('teacher_create_canvas_quiz_view')
        form = {
            'name': 'English Canvas Quiz Test',
            'course_id': '44',
            'quiz_id': '399',
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/create_quiz/confirm_quiz.html')
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Canvas Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Canvas Course ID': form['course_id'],
                                                          'Canvas Quiz ID': form['quiz_id'],
                                                          'Subject': self.subject,
                                                          'Time Limit (in minutes)': "0"})
        try:
            CanvasQuiz.objects.get(name=form['name'],
                                   course_id=form['course_id'],
                                   quiz_id=form['quiz_id'],
                                   subject=self.subject,
                                   time_limit=0)
        except CanvasQuiz.DoesNotExist:
            self.fail()

    def test_create_quiz_canvas_POST_error_invalid_course_id(self):
        client = self.login(self.user)

        url = reverse('teacher_create_canvas_quiz_view')
        form = {
            'name': 'English Canvas Quiz Test',
            'course_id': sys.maxsize,
            'quiz_id': '399',
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'], {
            'Canvas Course ID Error': 'The Canvas course ID must be a valid [Mathematics, General Ability, English] non revision course.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Canvas Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Canvas Course ID': str(form['course_id']),
                                                          'Canvas Quiz ID': form['quiz_id']})
        try:
            CanvasQuiz.objects.get(name=form['name'],
                                   course_id=form['course_id'],
                                   quiz_id=form['quiz_id'])
            self.fail()
        except CanvasQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_canvas_POST_error_invalid_quiz_id(self):
        client = self.login(self.user)

        url = reverse('teacher_create_canvas_quiz_view')
        form = {
            'name': 'English Canvas Quiz Test',
            'course_id': '44',
            'quiz_id': sys.maxsize,
        }
        response = client.post(url, form)
        session = client.session
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/invalid_quiz.html')
        self.assertEquals(response.context['errors'],
                          {'Canvas Quiz ID Error': 'The Canvas quiz ID must exist in the given course_id 44.'})
        self.assertEquals(response.context['form_data'], {'Quiz Type': 'Canvas Quiz',
                                                          'Quiz Name': form['name'],
                                                          'Canvas Course ID': form['course_id'],
                                                          'Canvas Quiz ID': str(form['quiz_id'])})
        try:
            CanvasQuiz.objects.get(name=form['name'],
                                   course_id=form['course_id'],
                                   quiz_id=form['quiz_id'])
            self.fail()
        except CanvasQuiz.DoesNotExist:
            # Pass test case does not exist
            True

    def test_create_quiz_canvas_example_GET(self):
        client = self.login(self.user)

        url = reverse('teacher_canvas_example_view')
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teacher/base.html')
        self.assertTemplateUsed(response, 'teacher/create_quiz/canvas_example.html')
