import json
import os

import django
import requests
from bs4 import BeautifulSoup
from canvasapi import Canvas

# we have to import Django settings before importing everything else
os.environ['DJANGO_SETTINGS_MODULE'] = 'threehundredselective.settings'
django.setup()

from quizengine.models import Subject
from quizengine.models import Difficulty
from quizengine.models import QuestionCategory
from quizengine.models import QuestionType
from quizengine.models import Question
from quizengine.models import Answer
from quizengine.models import Stimulus
from quizengine.models import SubjectToCategory

from quizconfig.models import QuizAnswers
from quizconfig.models import NormalQuiz
from quizconfig.models import NormalQuizResult
from quizconfig.models import CanvasQuiz
from quizconfig.models import CanvasQuizResult
from quizconfig.models import AdaptiveQuiz
from quizconfig.models import AdaptiveQuizResult
from quizconfig.models import PreviouslyWrongQuiz
from quizconfig.models import PreviouslyWrongQuizResult
from quizconfig.models import UnseenQuestionsQuiz
from quizconfig.models import UnseenQuestionsQuizResult

API_URL = "https://300selective.instructure.com/"
API_KEY = "13110~WKHvDb3G2WUFdEgvooCMatYyUYSo9qbxDXCE46hqT3ICPcVcJzVoDTziqDYzUeK6"
HEADERS = {'Authorization': 'Bearer {0}'.format(API_KEY)}


# Gets question JSON from Canvas API
def get_question(course_id, quiz_id, question_id):
    url = 'https://300selective.instructure.com/api/v1/courses/{0}/quizzes/{1}/questions/{2}'.format(
        course_id,
        quiz_id,
        question_id
    )
    return json.loads(requests.get(url, headers=HEADERS)._content)


# Adds a new subject
def populate_subject(subject):
    if subject is not None and subject != '':
        db_subject = None
        try:
            db_subject = Subject.objects.get(name=subject.lower())
        except Subject.DoesNotExist:
            db_subject = Subject(name=subject.lower())
            db_subject.save()
            print('Added Subject {0}'.format(db_subject.pk))
        return db_subject


# Adds a new difficulty
def populate_difficulty(difficulty, subject):
    if difficulty is not None and difficulty != '':
        db_difficulty = None
        try:
            db_difficulty = Difficulty.objects.get(
                name=difficulty.lower(),
                subject=subject,
                count=0
            )
        except Difficulty.DoesNotExist:
            db_difficulty = Difficulty(
                name=difficulty.lower(),
                subject=subject,
                count=0
            )
            db_difficulty.save()
            print('Added Difficulty {0}'.format(db_difficulty.pk))
        return db_difficulty


# Adds a new category
def populate_category(category):
    if category is not None and category != '':
        try:
            db_category = QuestionCategory.objects.get(name=category.lower().strip())
            return False, db_category
        except QuestionCategory.DoesNotExist:
            db_category = QuestionCategory(name=category.lower().strip())
            db_category.save()
            print('Added QuestionCategory {0}'.format(db_category.pk))
            return True, db_category
    return False, None


# Adds a new type
def populate_type(type):
    if type is not None and type != '':
        try:
            db_type = QuestionType.objects.get(name=type.lower().strip())
            return False, db_type
        except QuestionType.DoesNotExist:
            db_type = QuestionType(name=type.lower().strip())
            db_type.save()
            print('Added QuestionType {0}'.format(db_type.pk))
            return True, db_type
    return False, None


# Adds a new question
def populate_question(canvas_id, text, subject, difficulty, category, type, comments):
    if canvas_id is not None and text is not None and text != '':
        db_question = None
    try:
        db_question = Question.objects.get(
            canvas_id=canvas_id,
            text=text.strip(),
            subject=subject,
            difficulty=difficulty,
            category=category,
            type=type,
            comments=comments
        )
    except Question.DoesNotExist:
        db_question = Question(
            count_answered_correctly=0,
            count_answered_incorrectly=0,
            canvas_id=canvas_id,
            text=text.strip(),
            subject=subject,
            difficulty=difficulty,
            category=category,
            type=type,
            comments=comments
        )
        db_question.save()
        print('Added Question {0}'.format(db_question.pk))
    return db_question


# Add a new stimulus
def populate_stimulus(group_id, text):
    if group_id is not None and text is not None and text != '':
        db_stimulus = None
        try:
            db_stimulus = Stimulus.objects.get(
                group_id=group_id,
                text=text.strip()
            )
        except Stimulus.DoesNotExist:
            db_stimulus = Stimulus(
                group_id=group_id,
                text=text.strip()
            )
            db_stimulus.save()
            print('Added Stimulus {0}'.format(db_stimulus.pk))
        return db_stimulus


# Associates a question to a stimulus question variable
def associate_stimulus(group_id, question):
    if db_question is not None:
        try:
            db_stimulus = Stimulus.objects.get(group_id=group_id)
            db_stimulus.questions.add(question)
            print('Associated question {0} to stimulus {1}'.format(question.pk, db_stimulus.pk))
        except Stimulus.DoesNotExist:
            db_stimulus = None
        return db_stimulus


# Adds a new answer
def populate_answer(question, text, weighting):
    if text is not None and text != '' and weighting is not None:
        db_answer = None
        try:
            db_answer = Answer.objects.get(
                question=question,
                text=text,
                weighting=weighting
            )
        except Answer.DoesNotExist:
            db_answer = Answer(
                question=question,
                text=text,
                weighting=weighting
            )
            db_answer.save()
            print('Added Answer {0}'.format(db_answer.pk))
        return db_answer


# Deletes category from database
def abort_category(category):
    if category is not None:
        print('Abort remove QuestionCategory {0}'.format(category.pk))
        category.delete()


# Deletes type from database
def abort_type(type):
    if type is not None:
        print('Abort remove QuestionType {0}'.format(type.pk))
        type.delete()


# Deletes question from database
def abort_question(question):
    if question is not None:
        print('Abort remove Question {0}'.format(question.pk))
        question.delete()


# Deletes answers from database
def abort_answers(answers):
    for answer in answers:
        if answer is not None:
            print('Abort remove Answer {0}'.format(answer.pk))
            answer.delete()


print('---- RUNNING EXTRACTOR (WILL TAKE ~20 MINUTES) ----')

canvas = Canvas(API_URL, API_KEY)

subjects = ['mathematics', 'english', 'general ability']
difficulties = ['basic', 'easier', 'easy', 'hard', 'harder', 'challenge']

# Deletes existing
print('---- DELETE EXISTING DATA ----')
Subject.objects.all().delete()
Difficulty.objects.all().delete()
QuestionCategory.objects.all().delete()
QuestionType.objects.all().delete()
Question.objects.all().delete()
Answer.objects.all().delete()
Stimulus.objects.all().delete()

QuizAnswers.objects.all().delete()
NormalQuiz.objects.all().delete()
NormalQuizResult.objects.all().delete()
CanvasQuiz.objects.all().delete()
CanvasQuizResult.objects.all().delete()
AdaptiveQuiz.objects.all().delete()
AdaptiveQuizResult.objects.all().delete()
PreviouslyWrongQuiz.objects.all().delete()
PreviouslyWrongQuizResult.objects.all().delete()
UnseenQuestionsQuiz.objects.all().delete()
UnseenQuestionsQuizResult.objects.all().delete()

# Populate data
print('---- COLLECT CANVAS DATA ----')
for course in canvas.get_courses():
    for subject in subjects:
        # Avoid revision quizzes (repetition of questions)
        if subject in course.name.lower() and 'revision' not in course.name.lower():
            db_subject = populate_subject(subject)
            for quiz in course.get_quizzes():
                for difficulty in difficulties:
                    if difficulty == quiz.title.strip().lower():
                        db_difficulty = populate_difficulty(difficulty, db_subject)
                        # Get and insert stimulus question first so we can later associate
                        for question in quiz.get_questions():
                            question_data = get_question(course.id, quiz.id, question.id)
                            if question_data['question_name'].lower().strip() == 'stimulus':
                                db_stimulus = populate_stimulus(
                                    question_data['quiz_group_id'],
                                    question_data['question_text']
                                )

                                # Handle regular questions
                        for question in quiz.get_questions():
                            question_data = get_question(course.id, quiz.id, question.id)
                            if question_data['question_name'].lower().strip() != 'stimulus':
                                question_data = get_question(course.id, quiz.id, question.id)

                                # Flag is to signify if this is the first use of category or type i.e can it be deleted?
                                category_flag, db_category = populate_category(question_data['question_name'])

                                # This is to bind subject to category. (USED for random distribution.)
                                sub_to_cat = SubjectToCategory(subject=db_subject, category=db_category)

                                if len(SubjectToCategory.objects.filter(subject=db_subject).filter(
                                        category=db_category)) == 0:
                                    sub_to_cat.save()

                                type_flag, db_type = populate_type(question_data['question_type'])

                                comments = question_data['neutral_comments_html']

                                if db_category is not None and db_type is not None and comments is not None:
                                    db_question = populate_question(
                                        question_data['id'],
                                        question_data['question_text'],
                                        db_subject,
                                        db_difficulty,
                                        db_category,
                                        db_type,
                                        comments
                                    )
                                else:
                                    if category_flag:
                                        abort_category(db_category)
                                    if type_flag:
                                        abort_type(db_type)
                                    continue

                                db_answers = []
                                flag_abort_answers = False
                                if len(question_data['answers']) > 0 and db_question is not None:
                                    for answer in question_data['answers']:
                                        if answer['text'] == '' \
                                                or answer['text'] is None \
                                                or answer['text'] == 'No answer text provided.':
                                            soup = BeautifulSoup(answer['html'], 'html5lib')
                                            img = soup.find('img')
                                            if img is not None:
                                                request = requests.get(img['src'])
                                                if request.status_code != 200:
                                                    flag_abort_answers = True
                                                    break
                                            db_answers.append(populate_answer(
                                                db_question,
                                                answer['html'],
                                                answer['weight']
                                            ))
                                        else:
                                            db_answers.append(populate_answer(
                                                db_question,
                                                answer['text'].strip(),
                                                answer['weight']
                                            ))
                                else:
                                    flag_abort_answers = True

                                if flag_abort_answers:
                                    abort_question(db_question)
                                    if category_flag:
                                        abort_category(db_category)
                                    if type_flag:
                                        abort_type(db_type)
                                    if len(db_answers) > 0:
                                        abort_answers(db_answers)
                                    continue

                                if question_data['quiz_group_id'] is not None:
                                    db_stimulus = associate_stimulus(question_data['quiz_group_id'], db_question)
                                    if db_stimulus is None:
                                        abort_question(db_question)
                                        abort_answers(db_answers)
                                        if category_flag:
                                            abort_category(db_category)
                                        if type_flag:
                                            abort_type(db_type)

# Count the number of questions for a difficulty/subject (needed for forms)
for difficulty in Difficulty.objects.all():
    difficulty.count = len(Question.objects.all().filter(difficulty=difficulty))
    difficulty.save()
    print('Updated Difficulty {0} count'.format(difficulty.pk))

# Clean up empty subject/difficulty objects
for difficulty in Difficulty.objects.all():
    if difficulty.count == 0:
        print('Removed Difficulty {0}'.format(difficulty.pk))
        difficulty.delete()
