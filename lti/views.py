from django.conf import settings
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.contrib.auth.models import Permission
from lti_provider.mixins import LTIAuthMixin

from lti.models import UserType


@method_decorator(xframe_options_exempt, name='dispatch')
class LTIRoutingView(LTIAuthMixin, View):
    request_type = 'initial'
    role_type = 'any'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LTIRoutingView, self).dispatch(*args, **kwargs)

    def add_custom_parameters(self, url):
        if not hasattr(settings, 'LTI_EXTRA_PARAMETERS'):
            return url

        if '?' not in url:
            url += '?'
        else:
            url += '&'

        for key in settings.LTI_EXTRA_PARAMETERS:
            url += '{}={}&'.format(key, self.request.POST.get(key, ''))

        return url

    def post(self, request, assignment_name=None):
        if request.POST.get('ext_content_intended_use', '') == 'embed':
            domain = self.request.get_host()
            url = '%s://%s/%s?return_url=%s' % (
                self.request.scheme, domain,
                settings.LTI_TOOL_CONFIGURATION.get('embed_url'),
                request.POST.get('launch_presentation_return_url'))
        elif assignment_name:
            assignments = settings.LTI_TOOL_CONFIGURATION['assignments']
            url = assignments[assignment_name]
        elif settings.LTI_TOOL_CONFIGURATION.get('new_tab'):
            if self.lti.is_instructor(self.request):
                url = reverse('teacher_dashboard_view')
                if hasattr(request.user, 'usertype'):
                    if request.user.usertype.type == UserType.UNDEFINED:
                        request.user.usertype.type = UserType.TEACHER
                else:
                    usertype = UserType(user=request.user, type=UserType.TEACHER)
                    usertype.save()
                if not request.user.has_perm('lti.is_teacher'):
                    permission = Permission.objects.get(codename='is_teacher')
                    request.user.user_permissions.add(permission)
            else:
                url = reverse('student_dashboard_view')
                if hasattr(request.user, 'usertype'):
                    if request.user.usertype.type == UserType.UNDEFINED:
                        request.user.usertype.type = UserType.STUDENT
                else:
                    usertype = UserType(user=request.user, type=UserType.STUDENT)
                    usertype.save()
                if not request.user.has_perm('lti.is_student'):
                    permission = Permission.objects.get(codename='is_student')
                    request.user.user_permissions.add(permission)
        else:
            url = settings.LTI_TOOL_CONFIGURATION['landing_url'].format(
                self.request.scheme, self.request.get_host())

        # custom parameters can be tacked on here
        url = self.add_custom_parameters(url)

        return HttpResponseRedirect(url)
