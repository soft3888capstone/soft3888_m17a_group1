from django.urls import path
from .views import LTIRoutingView

urlpatterns = [
    path('lti/', LTIRoutingView.as_view()),
]
