from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class UserType(models.Model):
    UNDEFINED = 'U'
    TEACHER = 'T'
    STUDENT = 'S'
    TYPE_CHOICES = [
        (UNDEFINED, 'Undefined'),
        (TEACHER, 'Teacher'),
        (STUDENT, 'Student')
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, choices=TYPE_CHOICES, default=UNDEFINED)

    class Meta:
        permissions = [
            ("is_teacher", "Can access teacher content"),
            ("is_student", "Can access student content")
        ]
