import os

import django

# we have to import Django settings before importing everything else
os.environ['DJANGO_SETTINGS_MODULE'] = 'threehundredselective.settings'
django.setup()

from quizengine.models import Subject
from quizengine.models import Difficulty
from quizengine.models import QuestionCategory
from quizengine.models import QuestionType
from quizengine.models import Question
from quizengine.models import Answer
from quizengine.models import Stimulus

from quizconfig.models import QuizAnswers
from quizconfig.models import NormalQuiz
from quizconfig.models import NormalQuizResult
from quizconfig.models import CanvasQuiz
from quizconfig.models import CanvasQuizResult
from quizconfig.models import AdaptiveQuiz
from quizconfig.models import AdaptiveQuizResult
from quizconfig.models import PreviouslyWrongQuiz
from quizconfig.models import PreviouslyWrongQuizResult
from quizconfig.models import UnseenQuestionsQuiz
from quizconfig.models import UnseenQuestionsQuizResult

# Deletes existing
Subject.objects.all().delete()
Difficulty.objects.all().delete()
QuestionCategory.objects.all().delete()
QuestionType.objects.all().delete()
Question.objects.all().delete()
Answer.objects.all().delete()
Stimulus.objects.all().delete()

QuizAnswers.objects.all().delete()
NormalQuiz.objects.all().delete()
NormalQuizResult.objects.all().delete()
CanvasQuiz.objects.all().delete()
CanvasQuizResult.objects.all().delete()
AdaptiveQuiz.objects.all().delete()
AdaptiveQuizResult.objects.all().delete()
PreviouslyWrongQuiz.objects.all().delete()
PreviouslyWrongQuizResult.objects.all().delete()
UnseenQuestionsQuiz.objects.all().delete()
UnseenQuestionsQuizResult.objects.all().delete()
