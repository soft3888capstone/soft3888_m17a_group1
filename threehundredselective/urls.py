from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from issue import urls as issue_urls
from quizconfig import urls as quizconfig_urls
from quizengine import urls as quizengine_urls

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', include('lti.urls')),
                  path('lti/', include('lti_provider.urls')),
                  path('', include(quizengine_urls)),
                  path('', include(quizconfig_urls)),
                  path('', include(issue_urls)),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'quizengine.views.error404'
handler403 = 'quizengine.views.error403'
