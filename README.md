# README

## Sources/Code Utilised

* [HTML Django Template Query Dictionary Method (quizconfig\templatetages\dict_access.py, templates\teacher\create_quiz\distribution.html, templates\teacher\create_quiz\confirm_quiz.html)](https://stackoverflow.com/questions/57740685/in-a-django-template-how-do-i-print-the-value-in-a-dictionary-when-the-key-is-s)
* [Difficulty Dynamic Dropdown 1 (quizconfig\urls.py, quizconfig\views.py, quizconfig\forms.py, templates\teacher\create_quiz\normal_quiz.html, templates\teacher\create_quiz\difficulty_dropdown_list_options.html, static\scripts\teacher\get_difficulties.js)](https://github.com/akjasim/cb_dj_dependent_dropdown)
* [Difficulty Dynamic Dropdown 2 (quizconfig\urls.py, quizconfig\views.py, quizconfig\forms.py, templates\teacher\create_quiz\normal_quiz.html, templates\teacher\create_quiz\difficulty_dropdown_list_options.html, static\scripts\teacher\get_difficulties.js)](https://www.youtube.com/watch?v=LmYDXgYK1so)
* [Rounding 0.5 to 1.0 (quizconfig\views.py)](https://stackoverflow.com/questions/43851273/how-to-round-float-0-5-up-to-1-0-while-still-rounding-0-45-to-0-0-as-the-usual)
* [Breaking up request.path(issue\views.py, quizconfig\views.py, quizengine\views.py)](https://stackoverflow.com/questions/7669931/django-break-up-a-request-path-into-each-word)
* [Student Create Quiz Clipart (static\images\teacher\create.png)](https://www.pikpng.com/pngvi/iJmRJbR_png-file-edit-icon-circle-clipart/)
* [Student Edit Quiz Clipart (static\images\teacher\edit.png)](https://www.pinclipart.com/pindetail/JmTib_editor-edit-clipart-black-and-white-png-download/)
* [Student Results Clipart (static\images\teacher\results.png)](https://webstockreview.net/explore/data-clipart-site-analysis/)
* [Testing URLS Guide (quizconfig/tests/tests_urls.py) (issue/tests/tests_urls.py)](https://www.youtube.com/watch?v=0MrgsYswT1c)
* [Testing Views Guide (quizconfig/tests/tests_views.py) (issue/tests/tests_views.py)](https://www.youtube.com/watch?v=hA_VxnxCHbo)
* [Unit Testing a Django Form with a FileField (issue/tests/tests_views.py)](https://stackoverflow.com/questions/2473392/unit-testing-a-django-form-with-a-filefield)
* [Django Javascript Post Request (template/student/view_quizzes.html) (template/student/view_results.html) (template/student/view_attempts.html) (template/teacher/manage_quiz.html) (template/teacher/modify_quiz.html)](https://stackoverflow.com/questions/37869042/django-javascript-post-request)
* [How to pass CSRF Token to Javascript File in Django (template/student/view_quizzes.html) (template/student/view_results.html) (template/student/view_attempts.html) (template/teacher/manage_quiz.html) (template/teacher/modify_quiz.html)](https://stackoverflow.com/questions/23349883/how-to-pass-csrf-token-to-javascript-file-in-django)
* [render() not rendering template (template/student/view_quizzes.html) (template/student/view_results.html) (template/student/view_attempts.html) (template/teacher/manage_quiz.html) (template/teacher/modify_quiz.html)](https://stackoverflow.com/questions/22253844/render-not-rendering-template)
* [Redirecting after AJAX post in Django (template/student/view_quizzes.html) (template/student/view_results.html) (template/student/view_attempts.html) (template/teacher/manage_quiz.html) (template/teacher/modify_quiz.html)](https://stackoverflow.com/questions/29137910/redirecting-after-ajax-post-in-django)
* [Footer at bottom of page with dynamic content area (template/student/base.html) (template/teacher/base.html) (static/base_styles/style.css) (static/base_styles/style-config.css)](https://stackoverflow.com/questions/34062480/footer-at-bottom-of-page-with-dynamic-content-area)
* [Error Clipart (static/images/error.png)](https://iconsplace.com/white-icons/error-icon-18/)
* [Space between tags in a div (static/styles/teacher/manage_quiz/modify_quiz.css)](https://stackoverflow.com/questions/41407587/how-to-have-space-between-two-a-tag-in-the-same-div)
* [Delete popup using sweetalert2 (template/teacher/modify_quiz.html)](https://sweetalert2.github.io/)
* [Rename popup using sweetalert2 (template/teacher/modify_quiz.html)](https://sweetalert2.github.io/v7.html#input-types)
* [sweetalert2 example reports: await is only valid in async functions and async generators (template/teacher/modify_quiz.html)](https://stackoverflow.com/questions/47942149/sweetalert2-example-reports-await-is-only-valid-in-async-functions-and-async-ge)
* [Disabling back button on a page (templates/student/quiz-page-normal.html)](https://stackoverflow.com/a/34337617)
* [Confirmation before reloading a page (templates/student/quiz-page-normal.html)](https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload)

## Templates

Please reduce all edits to the `base*.*` files. Editing these files will modify all other pages.

## Issue APP

The issue APP is used for logging bug reports or issue reports made by users.

## QuizConfig APP

The quizconfig APP is used for the creation process of quiz objects and has distinct models and forms for such uses.

## QuizEngine APP

The quizengine APP is used for the quizzing process i.e storing questions, operating quiz, dashboards, quiz statistics.

## Django Workflow

[Documentation can be found here.](https://docs.djangoproject.com/en/3.0/)

### Starting Service

Navigate to `\master\new-project\threehundredselective`
`python manage.py runserver`
Open site in the console

### Migrate Changes

`python manage.py makemigrations`
`python manage.py migrate`
Ensure no errors afterwards

### Creating Superuser

`python manage.py createsuperuser`
[Admin page](http://127.0.0.1:8000/admin/)

### Create APP

`python manage.py startapp <appname>`
